.. dhpid documentation master file, created by
   sphinx-quickstart on Thu May 21 16:37:26 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DH-pid
======

The DH-pid service is only for internal use, no public API is provided.


Sources
-------

See dhpid_sources_


Bugtracking
-----------

See dhpid_bugtracking_


License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/blob/develop/LICENSE.txt
.. _dhpid_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services
.. _dhpid_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/-/issues?label_name[]=dh-pid
