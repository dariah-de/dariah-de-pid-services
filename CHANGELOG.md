## [5.7.3](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.7.2...v5.7.3) (2025-01-24)


### Bug Fixes

* .....moooore logging ([ba55e67](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/ba55e67f84a8c3b03916b8b93955087735eb5c68))
* add default endpoints and pathes ([5ec521a](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/5ec521aac80c4513c7a5b491211c231fd8ca8cca))
* add editorconfig ([fc5d448](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/fc5d4488c4b6196fbe50027b300c06c867d6d70a))
* add first new http client changes ([73b0730](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/73b07306448b64e2f4dafb3514cbf061ff8c1ad7))
* add logging for http headers ([bf2dce3](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/bf2dce311bcaffcef997f1a512d8d894511c9701))
* add online test method ([dc40f1a](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/dc40f1a94688239bc68e9b7e561722fd42261ea1))
* add spaces for tabs in pom files ([6c74143](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/6c74143bb9810340fe306ae99b13ae268dd33835))
* fix editorconfig ([9aeb25f](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/9aeb25f782e1612c92652782f319cff9b528949e))
* fix http headers? ([a635c80](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/a635c809b948c187155282fc41e09ace7648f129))
* fix pig typo in endpoint string ([0f9695b](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/0f9695b6dc8f88868ec03adf9b6d7ed586b08add))
* fix tests ([d5e3ad4](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/d5e3ad4fb02fdb504dbc4e5e3ec22b90c6c5b46e))
* fix update methods ([cb1114f](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/cb1114f34eec518b210c6e60db198df5d0977745))
* fix updates! ([43ef8b4](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/43ef8b47608da51fd7ece9f051e7c9160f2a4b13))
* log published dariah pid content with level fine ([36a8f55](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/36a8f55a888b744120eeff469218e48d93384492))
* remove hdl client version ([829332c](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/829332c5520e99427ecfa031e9cdafd765cb7094))
* remove tghttp clients, add java http clients ([8868b78](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/8868b788cacc25d212f4189b5ec6b91f75317097))
* remove unused hdllib ([0c0567b](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/0c0567b5b6091ae37e887ccb03274478149b601b))
* remove unused test class ([22f75db](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/22f75dbfbf477406a98876ed2dc8a77eab041d23))
* use json entity for updates now? ([14dba06](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/14dba06a927f5bd6d04e16d8f4ee3f3cc8c67ee5))

## [5.7.2](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.7.1...v5.7.2) (2024-11-25)


### Bug Fixes

* add logging to understand the problem ([fb517e1](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/fb517e156bca245bd8cf46e0a56e20672e7fc06e))
* back to tg client ([8245f4c](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/8245f4c107a792f4d5cef481cb22c03cbe5fc572))
* create new textgrid metadata also instead of update only ([71dd206](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/71dd20683f5f5fa16c0236ed3953adcb54d5e747))
* devide tg and hdl service existing metadata calls ([136b544](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/136b5441e9ba81acf7d40325a8a05d68cad1760a))
* fix creating new json array for hdl metadata ([b7db373](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/b7db373dbcc71129b5002fc4f8973b3329d6d2d2))
* fix hdl server path and auth ([0144fce](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/0144fceb838a04771a481267bb56c06e1a8b52b7))
* fix some metadata update issues (so at least i hope... :-) ([391a47f](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/391a47f54bd288943f90b9b5431357a3381d28c9))
* fix using array instead of object ([e359e8e](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/e359e8e81a05a4951a13f6ec14ef8c525e6a90e2))
* just re-set existing metadata (i think....) ([42def2f](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/42def2f08bd4f6bf13712583261c2121a5140dbf))
* re-set getExistingMeatdata to main ([d28eacd](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/d28eacd2b97a8425052d0b4a2c734bc60b0a6958))
* separate add and update metadata ([7bbe9a4](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/7bbe9a40c5739971816d27969b9a4c2d6e70fd2b))
* use hdl service insteda of pid.gwdg.de on getting metadata ([8d7ca5c](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/8d7ca5c142e459ec5b48a0ef2a6b00d48006d570))
* use pid.gwdg.de to get existing metadata ([e480104](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/e480104cce4abc82d981f8131b5d2380c6a8fc5b))

## [5.7.1](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.7.0...v5.7.1) (2024-09-04)


### Bug Fixes

* add more logging ([68ba261](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/68ba26156c0992b7319b1905afe79b64fc4a3ab8))
* add new method including tests ([80baa14](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/80baa140dfca0df71aa02bf4d71b35227aef3845))
* finish update hdl metadata procedure ([f9deb29](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/f9deb29dc9d91187bfbe8780f0b0e02134e3a992))
* fix dockerfile ([e62f573](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/e62f5739b096d469ddc300c16fdab8c67a0afd6a))
* fix method for metadata update ([f6b3205](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/f6b3205383a52c96c501d660e837de800fec659d))
* fix test ([26c5af6](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/26c5af66111b6b5e23117f7822696272a2b10b85))
* fix tests, finally ([d28eda8](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/d28eda888ad49b1a656efa1ee2df5e21fa6f0b07))
* fix typo ([92800ba](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/92800ba5cbc214d594465ca7ffa390ae76755f0e))
* fix typo ([9b542c9](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/9b542c983d910560d9c228b9f67148525a45d1c4))

# [5.7.0](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.6.0...v5.7.0) (2024-05-23)


### Bug Fixes

* add https namespace xsd locations in beans.xml ([91d7e06](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/91d7e06064433ee81376cd630f067eac879e3e14))
* add jakarta.ws-api ([930edf8](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/930edf8e0e98b14175aae3941082110cfbc77fac))
* add mvn-war-plugin plugin and version ([739fdda](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/739fddac18c9e1766c52a654ab084f450482c473))
* add some xsd files for xsd workout ([4d66c1a](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/4d66c1a2b36747ce9cd3eee33495008afbaf518c))
* add spring-context package ([7fa82a2](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/7fa82a2a124d89dc018632ade2f7192ddc222634))
* fix deps in pid-client ([27d5e02](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/27d5e02901f3b19450eae0de8e8049edfeeef948))
* fix more libs, add docker and ci updates ([ae9a2e7](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/ae9a2e7fec84a34fe508f283639e37a426bcfc18))
* increase some dep versions ([b87b112](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/b87b112ed8c3236481ba789558b677373dcb3d5a))
* increase tgauth version ([4094c99](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/4094c99b6c9aaf786eb972abf59b8a9412ec0f28))
* remove deb creation and deb config folders ([a37b1d4](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/a37b1d497f7524ab0f687feb7cf7742a1c24c596))
* remove unused dep ([c7b2a09](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/c7b2a09d0df4df528fa408de3c05821b4b657013))


### Features

* update to Java 17, adapting deps ([19e3021](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/19e3021ff89f63d9370f5320179c515d871e74a2))
* using tomcat 10 now, and spring 6 ([fc73c18](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/fc73c1852f4f7e8134615928f9aa82dd14c462db))

# [5.6.0](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.5.0...v5.6.0) (2023-02-28)


### Features

* drop aptly deployment ([1aa4ebb](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/1aa4ebb3467fb6c75825d3c130f7d422b140820e))

# [5.5.0](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/compare/v5.4.0...v5.5.0) (2022-07-07)


### Bug Fixes

* add BOM JSON generation ([c4d5701](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/c4d57014c938cb98facef196835fbd5a8f40a2b9))
* add contrib files ([88c9e3d](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/88c9e3dc52c1cc5451a14b90ead5bd18b11a13aa))
* add dry-run for deployment testing ([3e8bdd9](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/3e8bdd9c0b58975519c4f69ed504edb3c2cb2277))
* add new package things for new ci ([be82e01](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/be82e012508a9a589baa7d8776103d50fa98de9f))
* copy BOM file to / ([8e821ab](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/8e821abbb7012da49aa42d3e5d3ac21e5a93fa33))
* re-set pom version (again!) ([1a35e14](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/1a35e144f6ee0f92033f1695c399d280bb8d7103))
* re-set versions ([126a2d4](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/126a2d45fea07d1ccc4195042c1ebb2d95fba31f))
* remove dry-run from ci script ([f71c591](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/f71c591d31a465dc4f71b14a58f22495efbb5ba3))
* reset pom verion ([8a812b1](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/8a812b10683ff2dd3237ceafde886d1d3b87c3bb))


### Features

* Add new CI features ([1fdf3ca](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/1fdf3ca77e5d1347e8e1f9eff8c41e8eec1a56b4))
* add new CI features and workflow ([959ad52](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/commit/959ad52e31b7b0a8779a5b727b8ee8f413b49b95))

# 5.4.0-SNAPSHOT

* Adapt to new CI/CD workflow

# 5.3.1-SNAPSHOT

* Implement Gitlab CI/CD workflow
