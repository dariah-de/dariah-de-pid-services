# The DARIAH-DE PID Service

## Overview

The PID service is used for both TextGrid and the DARIAH-DE Repository, one instance for every installation.

## Documentation

For service and API documentation please have a look at the [TG-pid Documentation](https://textgridlab.org/doc/services/submodules/tg-pid/docs_tgrep/index.html), DH-pid is only used internally and does not need public documentation.

## Installation

The service is provided with a Gitlab CI workflow to build Docker images to be deployed and installed on TextGrid and DARIAH-DE servers.

### Building from GIT using Maven

You can check out the PID Service from our [GIT Repository](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services), develop or main branch, or get yourself a certain tag, and then use Maven to build the PID Service WAR file:

    git clone https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services.git

... build the WAR packages via:

    mvn clean package

You will get a PID Service WAR file in the folder ./pid-webapp/target.


## Deploying the PID Service

The service is deployed just by installing the appropriate Docker image.

## Links and References

* [PID Service Gitlab page](https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services)
