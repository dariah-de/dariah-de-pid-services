.. tgpid documentation master file, created by
   sphinx-quickstart on Thu May 21 16:37:26 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TG-pid
======
The TextGrid PID service is a wrapper for getting Persistent Identifiers (EPIC2 Handles) from the GWDG Handle Service. Most of the methods require authentication via a TextGrid RBAC session ID and are used by the `TextGrid Publish Service <tgpublish_doc_>`_ and the `TextGrid Import Tool <tgimport_doc_>`_.


RESTful Access
--------------


#ADDMETADATA
^^^^^^^^^^^^
This method just does add metadata to the existing PID key/value bindings.

**Parameters**

========  ==================================================================
Name      Description
========  ==================================================================
pid       The PID as a string
keyvalue  A list of strings containing key/value pairs, separated by a colon
========  ==================================================================


#GETDARIAHPID
^^^^^^^^^^^^^
TODO


#GETFAKEPIDS
^^^^^^^^^^^^
Returns a random UUID as fake PID for every URI given in the input String.

**Parameters**

========  ===================================================================
Name      Description
========  ===================================================================
uris      A string containing a bunch of URIs, separated by a colon
========  ===================================================================

**Result**

Returns a string of UUIDs for URIs such as

::

	textgrid:1234,textgrid:2345,textgrid:3456

as fake PIDs in the form

::

	textgrid:1234@7e48e1d6-1995-407b-b054-2d2df9c01e3f,textgrid:2345@784cfe82-62e8-4c41-aea8-b0544cbed486,textgrid:3456@39ec4d6a-56f9-4f1e-a188-36e2d230fbb1


#GETPID
^^^^^^^

**Parameters**

========  ===================================================================
Name      Description
========  ===================================================================
sid       A TextGrid RBAC Session ID
log       A TextGrid log parameter
uri       The URI to resolve the PID to
check     If set to true, the existing PID is returned for the given URI,
          a new PID is generated otherwise
========  ===================================================================

**Result**

TODO


#GETEXTENDEDPID
^^^^^^^^^^^^^^^

**Parameters**

=========  ===================================================================
Name       Description
=========  ===================================================================
sid        A TextGrid RBAC Session ID
log        A TextGrid log parameter
uri        The URI to resolve the PID to
filesize   The object's filesize (metadata) to get the PID for
checksum   The objects's checksum (metadata) to get the PID for
publisher  The object's publisher (metadata) to get the PID for
check      If set to true, the existing PID is returned for the given URI,
           a new PID is generated otherwise
=========  ===================================================================

**Result**


#GETPIDS
^^^^^^^^

**Parameters**

=========  ===================================================================
Name       Description
=========  ===================================================================
sid        A TextGrid RBAC Session ID
log        A TextGrid log parameter
uris       The URI list to resolve the PIDs to, separated by a colon
check      If set to true, the existing PIDs are returned for the given URIs,
           new PIDs are generated otherwise
=========  ===================================================================

**Result**


#GETEXTENDEDPIDS
^^^^^^^^^^^^^^^^

**Parameters**

=========  ===================================================================
Name       Description
=========  ===================================================================
sid        A TextGrid RBAC Session ID
log        A TextGrid log parameter
uri        The URI to resolve the PID to
filesize   The object's filesizes (metadata), separated by a colon
checksum   The objects's checksums (metadata), separated by a colon
publisher  The object's publishers (metadata), separated by a colon
check      If set to true, the existing PIDs are returned for the given URIs,
           new PIDs are generated otherwise
=========  ===================================================================

**Result**


#getUri
^^^^^^^

**Parameters**

**Result**


#PUBLISHEDDARIAHPID
^^^^^^^^^^^^^^^^^^^
TODO


#REGISTEREDDARIAHPID
^^^^^^^^^^^^^^^^^^^^
TODO


#SEARCHPID
^^^^^^^^^^

**Parameters**

========  ===================================================================
Name      Description
========  ===================================================================
log       A TextGrid log parameter
uri       The URI to resolve the PID to
========  ===================================================================

**Result**


#SEARCHDARIAHPID
^^^^^^^^^^^^^^^^
TODO


#UPDATEPID
^^^^^^^^^^
TODO


#UPDATEMETADATA
^^^^^^^^^^^^^^^
TODO


#UPDATETEXTGRIDMETADATA
^^^^^^^^^^^^^^^^^^^^^^^
TODO


#VERSION
^^^^^^^^
The returned version string consists of BUILDNAME, VERSION, and BUILDDATE, such as

::

	2.9.5-SNAPSHOT-2014-07-28T13:26-'EPIC PID v2'

Just try https://textgridlab.org/1.0/tgpid/version.


Sources
-------

See tgpid_sources_


Bugtracking
-----------

See tgpid_bugtracking_


License
-------

See LICENCE_

.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/blob/develop/LICENSE.txt
.. _tgpid_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services
.. _tgpid_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-pid-services/-/issues?label_name[]=tg-pid
.. _tgpublish_doc: https://textgridlab.org/doc/services/submodules/kolibri/kolibri-tgpublish-service/docs/index.html
.. _tgimport_doc: https://textgridlab.org/doc/services/submodules/kolibri/kolibri-addon-textgrid-import/docs/index.html
