/**
 * This software is copyright (c) 2025 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.api;

import java.util.List;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * <p>
 * Interface used for TextGrid and DARIAH-DE PID API.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2025-02-03
 * @since 2011-07-04
 */

public interface PidService {

  public static final String SID_HEADER_PARAM = "X-RBAC-SID";
  public static final String AUTH_HEADER_PARAM = "X-PID-SECRET";
  public static final String URI_PARAM = "uri";
  public static final String URIS_PARAM = "uris";
  public static final String PID_PARAM = "pid";
  public static final String PIDS_PARAM = "pids";
  public static final String PIDSUFFIX_PARAM = "pidSuffix";
  public static final String PIDSUFFIXES_PARAM = "pidSuffixes";
  public static final String FILESIZE_PARAM = "filesize";
  public static final String CHECKSUM_PARAM = "checksum";
  public static final String PUBLISHER_PARAM = "publisher";
  public static final String DOI_PARAM = "doi";
  public static final String DOIMETA_PARAM = "doimeta";
  public static final String EPPN_PARAM = "eppn";
  public static final String INFO_PARAM = "info";
  public static final String DCMETA_PARAM = "dc";
  public static final String TYPE_PARAM = "type";

  /**
   * <p>
   * The PID service version.
   * </p>
   * 
   * @return The PID service version string.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/version")
  public String version();

  /**
   * <p>
   * Get a PID for given URI from given PID suffix, authentication for use of PID service is
   * validated with SID (creates new PID!).
   * </p>
   * 
   * @param info A Handle info object, holding PID, suffix, and metadata.
   * @param sid A TextGrid RBAC session ID.
   * @return A Handle info object holding PID and suffix only (no metadata!).
   * @throws PidServiceException
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/getTextgridPid")
  public HandleInfo getTextgridPid(HandleInfo info,
      @HeaderParam(SID_HEADER_PARAM) String sid)
      throws PidServiceException;

  /**
   * <p>
   * Get PID for URIs, authentication for use of PID service is validated with SID (creates NEW PIDs
   * for given URIs!).
   * </p>
   * 
   * @param infos A List of Handle info objects, holding PID, suffix, and metadata.
   * @param sid A TextGrid RBAC session ID.
   * @return A list of Handle info objects holding PID and suffix only (no metadata!).
   * @throws PidServiceException
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/getTextgridPids")
  public List<HandleInfo> getTextgridPids(List<HandleInfo> infos,
      @HeaderParam(SID_HEADER_PARAM) String sid)
      throws PidServiceException;

  /**
   * <p>
   * Get PID for DARIAH.
   * </p>
   * 
   * @param info A Handle info object, holding PID, suffix, and metadata.
   * @param auth The secret to use the PID service.
   * @return A Handle info object holding PID and suffix only (no metadata!).
   * @throws PidServiceException
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/getDariahPid")
  public HandleInfo getDariahPid(HandleInfo info,
      @HeaderParam(AUTH_HEADER_PARAM) String auth)
      throws PidServiceException;

  // /**
  // * <p>
  // * Get PID for DARIAH.
  // * </p>
  // *
  // * @param auth The secret to use the PID service.
  // * @return A Handle info object holding PID and suffix only (no metadata!).
  // * @throws TGPidServiceException
  // */
  // @POST
  // @Produces(MediaType.APPLICATION_JSON)
  // @Path("/getDariahPid")
  // public HandleInfo getDariahPid(@QueryParam(AUTH_PARAM) String auth)
  // throws TGPidServiceException;

  /**
   * <p>
   * Get DOI for DARIAH.
   * </p>
   * 
   * @param pid The PID to create the DOI from, we use the PID suffix for DOI suffix. The DOI is
   *        resolved via Handle service and the given Handle PID.
   * @param doiMeta The DOI metadata to set.
   * @param auth The secret to use the PID service.
   * @return DOI A newly created DOI string.
   * @throws PidServiceException
   */
  @POST
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getDariahDoi")
  public String getDariahDoi(@FormParam(PID_PARAM) String pid,
      @FormParam(DOIMETA_PARAM) String doiMetadata,
      @HeaderParam(AUTH_HEADER_PARAM) String auth)
      throws PidServiceException;

  /**
   * <p>
   * Get DOI for TextGrid.
   * </p>
   * 
   * @param sid A TextGrid RBAC session ID.
   * @param pid The suffix to create the DOI from. The DOI is resolved via Handle service and the
   *        given Handle PID.
   * @param doiMeta The DOI metadata to set.
   * @return DOI A newly created DOI string.
   */
  @POST
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getTextgridDoi")
  public String getTextgridDoi(@FormParam(PID_PARAM) String pid,
      @FormParam(DOIMETA_PARAM) String doiMeta,
      @HeaderParam(SID_HEADER_PARAM) String sid);

  /**
   * <p>
   * Returns true if the PUBLISHED metadata (type) of a given PID does exist and it's value is
   * "true", false otherwise. Used to check if a URI already has been published to the DARIAH-DE
   * Repository.
   * </p>
   *
   * <p>
   * Example: https://hdl.handle.net/api/handles/21.T11991/0000-001B-41BD-5?type=PUBLISHED
   * </p>
   * 
   * @param pid The PID to check the PUBLISHED status for.
   * @return true if Handle contains PUBLISHED=true, false otherwise
   * @throws PidServiceException
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/publishedDariahPid")
  public boolean publishedDariahPid(@QueryParam(PID_PARAM) String pid)
      throws PidServiceException;

  /**
   * <p>
   * Gets the PIDs Handle metadata value for the given type.
   * </p>
   *
   * <p>
   * Example: https://hdl.handle.net/api/handles/21.T11991/0000-001B-41BD-5?type=DELETED
   * </p>
   * 
   * @param pid The PID to query the type metadata for.
   * @param type The metadata type to get the value for.
   * @return PID metadata value string of given TYPE, if existing, empty String otherwise.
   * @throws PidServiceException
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getMetadata")
  public String getMetadata(@QueryParam(PID_PARAM) String pid,
      @QueryParam(TYPE_PARAM) String type)
      throws PidServiceException;

  /**
   * <p>
   * Updates all the given PID metadata types and values. Existing values are overwritten, existing
   * but not given types stay unchanged, not yet existing types are NOT created automatically!
   * </p>
   * 
   * @param info A Handle info object, holding PID and metadata to update.
   * @param A map of key/value pairs to update/create.
   * @param auth The secret to use the PID service.
   * @throws PidServiceException
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/updateMetadata")
  public void updateMetadata(HandleInfo info,
      @HeaderParam(AUTH_HEADER_PARAM) String auth)
      throws PidServiceException;

  /**
   * <p>
   * Just adds new metadata to the existing type values. Already existing types are NOT added
   * (again) and NOT added!
   * </p>
   * 
   * @param info A Handle info object, holding PID and metadata to add.
   * @param auth
   * @throws PidServiceException
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/addMetadata")
  public void addMetadata(HandleInfo info,
      @HeaderParam(AUTH_HEADER_PARAM) String auth)
      throws PidServiceException;

  /**
   * <p>
   * Get FAKE PIDs!! No auth necessary.
   * </p>
   * 
   * @param infos A List of Handle info objects, holding PID, suffix, and metadata.
   * @return Comma separated list string of fake PIDs.
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/getFakePids")
  public List<HandleInfo> getFakePids(List<HandleInfo> infos);

  /**
   * <p>
   * Query URI belonging to PID.
   * </p>
   * 
   * @param pid The PID to get the resolving URL for.
   * @return The URL string this PID resolves to if existing, empty string otherwise.
   * @throws PidServiceException
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getUri")
  public String getUri(@QueryParam(PID_PARAM) String pid)
      throws PidServiceException;

}
