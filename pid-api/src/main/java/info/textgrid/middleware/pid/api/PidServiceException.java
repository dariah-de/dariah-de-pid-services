/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.api;

import java.io.IOException;

/**
 * <p>
 * PID service exception class.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2025-01-16
 * @since 2025-01-16
 */

public class PidServiceException extends Exception {

  private static final long serialVersionUID = 1L;

  /**
   * @param e
   */
  public PidServiceException(IOException e) {
    super(e);
  }

  /**
   * @param e
   */
  public PidServiceException(String m) {
    super(m);
  }

}
