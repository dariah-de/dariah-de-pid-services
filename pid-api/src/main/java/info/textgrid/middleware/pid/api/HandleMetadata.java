/**
 * This software is copyright (c) 2025 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.middleware.pid.api;

import java.util.HashMap;

/**
 * <p>
 * Handle Information pojo for Handle Metadata.
 * </p>
 */

public class HandleMetadata {

  // **
  // FINALS
  // **

  // Generic
  public static final String MDTYPE_URL = "URL";
  public static final String MDTYPE_FILESIZE = "FILESIZE";
  public static final String MDTYPE_CHECKSUM = "CHECKSUM";
  public static final String MDTYPE_PUBDATE = "PUBDATE";
  public static final String MDTYPE_CREATOR = "CREATOR";
  public static final String MDTYPE_DOI = "DOI";
  public static final String MDTYPE_DELETED = "DELETED";

  // TGREP
  public static final String MDTYPE_TG_METADATA = "METADATA_URL";
  public static final String MDTYPE_TG_PUBLISHER = "PUBLISHER";

  // DHREP
  public static final String MDTYPE_DH_METADATA = "METADATA";
  public static final String MDTYPE_DH_ADMMD = "ADM_MD";
  public static final String MDTYPE_DH_TECHMD = "TECH_MD";
  public static final String MDTYPE_DH_PROVMD = "PROV_MD";
  public static final String MDTYPE_DH_BAG = "BAG";
  public static final String MDTYPE_DH_DATA = "DATA";
  public static final String MDTYPE_DH_LANDING = "LANDING";
  public static final String MDTYPE_DH_SOURCE = "SOURCE";
  public static final String MDTYPE_DH_RESPONSIBLE = "RESPONSIBLE";
  public static final String MDTYPE_DH_PUBLISHED = "PUBLISHED";

  // **
  // CLASS
  // **

  // General
  public String url;
  public String filesize;
  public String checksum;
  public String pubdate;
  public String creator;
  public String doi;
  // TGREP
  public String tgMetadata;
  public String tgPublisher;
  // DHREP
  public String dhMetadata;
  public String dhAdmmd;
  public String dhTechmd;
  public String dhProvmd;
  public String dhBag;
  public String dhData;
  public String dhLanding;
  public String dhSource;
  public String dhResponsible;
  public String dhPublished;

  /**
   * 
   */
  public HandleMetadata() {
    //
  }

  /**
   * @param url The URL to resolve this Handle to.
   */
  public HandleMetadata(final String url) {
    this.url = url;
  }

  /**
   * @return Returns A HashMap with all the Handle metadata.
   */
  public HashMap<String, String> getMetadataMap() {

    HashMap<String, String> result = new HashMap<String, String>();
    // Put in general attributes.
    if (this.url != null && !this.url.isBlank()) {
      result.put(MDTYPE_URL, this.url);
    }
    if (this.filesize != null && !this.filesize.isBlank()) {
      result.put(MDTYPE_FILESIZE, this.filesize);
    }
    if (this.checksum != null && !this.checksum.isBlank()) {
      result.put(MDTYPE_CHECKSUM, this.checksum);
    }
    if (this.pubdate != null && !this.pubdate.isBlank()) {
      result.put(MDTYPE_PUBDATE, this.pubdate);
    }
    if (this.creator != null && !this.creator.isBlank()) {
      result.put(MDTYPE_CREATOR, this.creator);
    }
    if (this.doi != null && !this.doi.isBlank()) {
      result.put(MDTYPE_DOI, this.doi);
    }
    // Put in TGREP attributes.
    if (this.tgMetadata != null && !this.tgMetadata.isBlank()) {
      result.put(MDTYPE_TG_METADATA, this.tgMetadata);
    }
    if (this.tgPublisher != null && !this.tgPublisher.isBlank()) {
      result.put(MDTYPE_TG_PUBLISHER, this.tgPublisher);
    }
    // Put in DHREP attributes.
    if (this.dhMetadata != null && !this.dhMetadata.isBlank()) {
      result.put(MDTYPE_DH_METADATA, this.dhMetadata);
    }
    if (this.dhAdmmd != null && !this.dhAdmmd.isBlank()) {
      result.put(MDTYPE_DH_ADMMD, this.dhAdmmd);
    }
    if (this.dhTechmd != null && !this.dhTechmd.isBlank()) {
      result.put(MDTYPE_DH_TECHMD, this.dhTechmd);
    }
    if (this.dhProvmd != null && !this.dhProvmd.isBlank()) {
      result.put(MDTYPE_DH_PROVMD, this.dhProvmd);
    }
    if (this.dhBag != null && !this.dhBag.isBlank()) {
      result.put(MDTYPE_DH_BAG, this.dhBag);
    }
    if (this.dhData != null && !this.dhData.isBlank()) {
      result.put(MDTYPE_DH_DATA, this.dhData);
    }
    if (this.dhLanding != null && !this.dhLanding.isBlank()) {
      result.put(MDTYPE_DH_LANDING, this.dhLanding);
    }
    if (this.dhSource != null && !this.dhSource.isBlank()) {
      result.put(MDTYPE_DH_SOURCE, this.dhSource);
    }
    if (this.dhResponsible != null && !this.dhResponsible.isBlank()) {
      result.put(MDTYPE_DH_RESPONSIBLE, this.dhResponsible);
    }
    if (this.dhPublished != null && !this.dhPublished.isBlank()) {
      result.put(MDTYPE_DH_PUBLISHED, this.dhPublished);
    }

    return result;
  }

  /**
   *
   */
  public String toString() {
    return this.getMetadataMap().toString();
  }

}
