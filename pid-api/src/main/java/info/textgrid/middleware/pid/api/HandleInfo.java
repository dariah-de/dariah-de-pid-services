/**
 * This software is copyright (c) 2025 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.middleware.pid.api;

/**
 * <p>
 * General PID Information pojo, holds Handle PID, suffix, and Handle metadata.
 * </p>
 */

public class HandleInfo {

  public String suffix;
  public String handle;
  public HandleMetadata metadata;

  /**
   * 
   */
  public HandleInfo() {
    this.metadata = new HandleMetadata();
  }

  /**
   * <p>
   * Create a new Handle Info Object. Both suffix and URL is mandatory for creation!
   * </p>
   *
   * @param suffix The suffix to create the Handle from.
   * @param url The URL to resolve to.
   */
  public HandleInfo(String suffix, String url) {
    this.suffix = suffix;
    this.metadata = new HandleMetadata(url);
  }

  /**
   *
   */
  public String toString() {
    return "{sfx=" + (this.suffix == null ? "null" : this.suffix)
        + ", hdl=" + (this.handle == null ? "null" : this.handle)
        + "}";
  }

}
