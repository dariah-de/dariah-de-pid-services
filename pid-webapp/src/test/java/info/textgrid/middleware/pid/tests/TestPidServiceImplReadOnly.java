/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.pid.PidServiceImpl;
import info.textgrid.middleware.pid.api.HandleInfo;
import info.textgrid.middleware.pid.api.PidServiceException;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;

/**
 * <p>
 * Test class for testing PID service implementation. Tests read only methods only, we have no auth
 * here. Please see online test classes for online tests!
 * </p>
 */

public class TestPidServiceImplReadOnly {

  private static String prefix = "12.3456";
  private static String pidUser = "pid_user";
  private static String pidIndex = "1";
  private static String pidPassword = "pid_password";
  private static String secret = "secret";
  private static String logLocation = "./krams.log";
  private static String doiPrefix = "10.20375";
  private static String doiServer = "https://doi.org";
  private static String doiUser = "doi_user";
  private static String doiPassword = "doi_password";
  private static String doiTargetUrl = "doi_target_url";
  private static String doiPublisher = "doi_publisher";
  private static String logLevel = "INFO";
  private static PortTgextra tgauth = null;

  static PidServiceImpl pidService;

  /**
   * <p>
   * Create PID Service instance.
   * </p>
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    pidService =
        new PidServiceImpl(prefix, pidUser, pidIndex, pidPassword, secret, tgauth, logLocation,
            doiPrefix, doiServer, doiUser, doiPassword, doiTargetUrl, doiPublisher, logLevel);
  }

  /**
   * 
   */
  @Test
  public void testVersion() {

    System.out.println("testVersion()");

    String version = pidService.version();

    System.out.println("\t" + version);
  }

  /**
   * @return
   */
  @Test
  @Ignore
  public void testGetPid() {}

  /**
   * @return
   */
  @Test
  @Ignore
  public void testGetHandleMetadata() {}

  /**
   * @return
   */
  @Test
  @Ignore
  public void testUpdateMetadata() {}

  /**
   * @return
   */
  @Test
  @Ignore
  public void testUpdateTextGridMetadata() {}

  /**
   * 
   */
  @Test
  @Ignore
  public void testAddMetadata() {}

  /**
   * @return
   */
  @Test
  @Ignore
  public void testGetPids() {}

  /**
   * 
   */
  @Test
  public void testGetFakePids() {

    System.out.println("testGetFakePids()");

    List<HandleInfo> infoList = new ArrayList<HandleInfo>();
    HandleInfo info1 = new HandleInfo("1234.5", "https://textgrid:1234.5");
    infoList.add(info1);
    HandleInfo info2 = new HandleInfo("2345.6", "https://textgrid:2345.6");
    infoList.add(info2);
    HandleInfo info3 = new HandleInfo("3456.7", "https://textgrid:3456.7");
    infoList.add(info3);
    HandleInfo info4 = new HandleInfo("4567.8", "https://textgrid:4567.8");
    infoList.add(info4);

    System.out.println("\tincoming: " + infoList);

    List<HandleInfo> fakeUriList = pidService.getFakePids(infoList);

    System.out.println("\tcreated:  " + fakeUriList);

    assertTrue(fakeUriList.get(0).handle.equals("12.3456/1234.5"));
    assertTrue(fakeUriList.get(1).handle.equals("12.3456/2345.6"));
    assertTrue(fakeUriList.get(2).handle.equals("12.3456/3456.7"));
    assertTrue(fakeUriList.get(3).handle.equals("12.3456/4567.8"));
  }

  /**
   * @throws PidServiceException
   */
  @Test
  public void testGetUriTG() throws PidServiceException {

    System.out.println("testGetUriTG()");

    String pid = "21.11113/00-1734-0000-0005-1424-B";
    String expected = "http://textgridrep.org/textgrid:vqn2.0";

    String uri = pidService.getUri(pid);

    System.out.println("\t" + pid + " -> " + uri);

    assertTrue(uri.equals(expected));
  }

  /**
   * @throws PidServiceException
   */
  @Test
  public void testGetUriDH() throws PidServiceException {

    System.out.println("testGetUriDH()");

    String pid = "21.11113/0000-000B-D265-6";
    String expected = "https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-D265-6";

    String uri = pidService.getUri(pid);

    System.out.println("\t" + pid + " -> " + uri);

    assertTrue(uri.equals(expected));
  }

}
