/**
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;
import info.textgrid.middleware.pid.PidServiceUtils;
import info.textgrid.middleware.pid.api.HandleInfo;
import info.textgrid.middleware.pid.api.HandleMetadata;
import info.textgrid.middleware.pid.api.PidService;
import info.textgrid.middleware.pid.api.PidServiceException;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;

/**
 * <p>
 * Tests most of the DARIAH-DE PID Service methods online. Some DH-pid Service methods can not be
 * tested here, because they are only internally available. Needs accurate properties file for
 * complete test run.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class TestPidServiceOnline {

  // **
  // FINAL
  // **

  private static final String DEFAULT_PROPERTIES = "./src/test/resources/test.properties";
  // If the PROPERTIES file is not found, DEFAULT_PROPERTIES file is being used, no methods with
  // authentication are tested then!
  private static final String PROPERTIES = "./src/test/resources/DEV.test.properties";
  // private static final String PROPERTIES = "./src/test/resources/TEST.test.properties";
  // private static final String PROPERTIES = "./src/test/resources/LOCAL.test.properties";

  // **
  // STATIC
  // **

  // The PID Service client for creating and updating Handle PIDs.
  private static PidService pidService;
  private static String pidEndpoint;
  private static String rbacSid;
  private static String pidSecret;
  // The Handle Service Client used for resolving and checking Handle PIDs.
  private static HandleResolver hdlResolver = new HandleResolver();
  // Set auth testing default to true.
  private static boolean testMethodsWithAuthentication = true;

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Load properties file and properties.
    File conf = new File(PROPERTIES);

    System.out.print("looking for config file " + conf.getName() + "...");

    if (!conf.exists()) {
      conf = new File(DEFAULT_PROPERTIES);

      System.out.println("not found");
      System.out.println("using default config file " + conf.getName());

      // Do not test methods that need authentication if default config file is used! There are no
      // auth values!
      testMethodsWithAuthentication = false;

      System.out.println("auth method testing disabled");

    } else {
      System.out.println("found");
    }

    System.out.println("loading config");

    Properties p = new Properties();
    p.load(new FileInputStream(conf));

    pidEndpoint = p.getProperty("PID_ENDPOINT");
    rbacSid = p.getProperty("SID");
    pidSecret = p.getProperty("PID_SECRET");

    // Init TG-pid client.
    initTgpid(pidEndpoint);
  }

  /**
   * <p>
   * Test version string, see https://textgridlab.org/1.0/tgpid/version.
   * </p>
   */
  @Test
  public void testGetVersion() {

    System.out.println("testGetVersion()");

    String version = pidService.version();

    System.out.println("\tversion: " + version);

    assertTrue(version != null && !version.isEmpty());
  }

  /**
   * <p>
   * Test if a dhrep pid has the type PUBLISHED=true.
   * </p>
   * 
   * @throws PidServiceException
   */
  @Test
  public void testPublishedDariahPid() throws PidServiceException {

    System.out.println("testPublishedDariahPid()");

    if (testMethodsWithAuthentication) {
      String pid = "21.T11991/0000-0011-B8C6-E";
      boolean expected = true;
      boolean published = pidService.publishedDariahPid(pid);

      System.out.println("\tpublished: " + published);

      assertTrue(published == expected);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test getting handle metadata for type=URL from PID service.
   * </p>
   * 
   * @throws PidServiceException
   */
  @Test
  public void testGetHandleMetadata() throws PidServiceException {

    System.out.println("testGetHandleMetadata()");

    if (testMethodsWithAuthentication) {
      String pid = "21.T11991/0000-001B-5090-5";
      String expected = "https://textgridrep.org/textgrid:3tmfz.0";
      String url = pidService.getMetadata(pid, HandleMetadata.MDTYPE_URL);

      System.out.println("\tmetadata url for " + pid + ": " + url);

      assertTrue(url.equals(expected));

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test getting fake PIDs.
   * </p>
   * 
   * @throws PidServiceException
   */
  @Test
  public void testGetFakePids() throws PidServiceException {

    System.out.println("testGetFakePids()");

    if (testMethodsWithAuthentication) {
      List<HandleInfo> infoList = new ArrayList<HandleInfo>();
      HandleInfo info1 = new HandleInfo("1234.5", "http://fugu.de/1234.5");
      infoList.add(info1);
      HandleInfo info2 = new HandleInfo("2345.6", "http://fugu.de/2345.6");
      infoList.add(info2);
      HandleInfo info3 = new HandleInfo("3456.7", "http://fugu.de/3456.7");
      infoList.add(info3);

      System.out.println("\tincoming: " + infoList);

      List<HandleInfo> fakePids = pidService.getFakePids(infoList);

      System.out.println("\tfake pids: " + fakePids);

      System.out.println(fakePids);

      // FIXME Check response!

      // assertTrue(fakePids.contains("textgrid:1234.5@21.T11991/"));
      // assertTrue(fakePids.contains("textgrid:2345.6@21.T11991/"));
      // assertTrue(fakePids.contains("textgrid:3456.7@21.T11991/"));

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test getting the value for type URL.
   * </p>
   * 
   * @throws PidServiceException
   */
  @Test
  public void testGetUri() throws PidServiceException {

    System.out.println("testGetUri()");

    if (testMethodsWithAuthentication) {
      String pid = "21.T11991/0000-001B-5090-5";
      String expected = "https://textgridrep.org/textgrid:3tmfz.0";
      String uri = pidService.getUri(pid);

      System.out.println("\turi for " + pid + ": " + uri);

      assertTrue(uri.equals(expected));

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test getting a new PID.
   * </p>
   * 
   * @throws PidServiceException
   * @throws HandleException
   */
  @Test
  public void testGetTextgridPid() throws PidServiceException, HandleException {

    System.out.println("testGetTextgridPid()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        String suffix = UUID.randomUUID().toString();
        String url = "https://textgridrep.org/junit";
        String filesize = "1234";
        String checksum = "md5:1234567890";
        String publisher = "fuuguu";

        HandleInfo info = new HandleInfo(suffix, url);
        info.metadata.filesize = filesize;
        info.metadata.checksum = checksum;
        info.metadata.tgPublisher = publisher;

        String pid = getTextgridPid(info);

        checkTextgridPid(pid, url);

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  /**
   * <p>
   * Test getting an extended PID.
   * </p>
   * 
   * @throws PidServiceException
   * @throws HandleException
   */
  @Test
  public void testGetExtendedTextgridPid() throws PidServiceException, HandleException {

    System.out.println("testGetExtendedTextGridPid()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        String suffix = UUID.randomUUID().toString();
        String url = "https://textgridrep.org/junit";
        String filesize = "123456";
        String checksum = "md5:436997d3448721afc1c19dc3de2ae780";
        String publisher = "TextGrid";

        HandleInfo info = new HandleInfo(suffix, url);
        info.metadata.filesize = filesize;
        info.metadata.checksum = checksum;
        info.metadata.tgPublisher = publisher;

        String pid = getTextgridPid(info);

        checkExtendedTextgridPid(pid,
            url,
            filesize,
            checksum,
            publisher);

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  /**
   * <p>
   * Test getting a number of extended PIDs.
   * </p>
   * 
   * @throws PidServiceException
   */
  @Test
  public void testGetTextGridPids() throws PidServiceException {

    System.out.println("testGetExtendedTextGridPids()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        String suffix1 = UUID.randomUUID().toString();
        String suffix2 = UUID.randomUUID().toString();
        String url1 = "https://textgridrep.org/junitU1";
        String url2 = "https://textgridrep.org/junitU2";
        String filesize1 = "123456";
        String filesize2 = "234567";
        String checksum1 = "md5:436997d3448721afc1c19dc3de2ae780";
        String checksum2 = "md5:260d3eeff80ca065c2ac4bdde282ac52";
        String publisher = "TextGrid";

        List<HandleInfo> infoList = new ArrayList<HandleInfo>();
        HandleInfo info1 = new HandleInfo(suffix1, url1);
        info1.metadata.filesize = filesize1;
        info1.metadata.checksum = checksum1;
        info1.metadata.tgPublisher = publisher;
        infoList.add(info1);
        HandleInfo info2 = new HandleInfo(suffix2, url2);
        info2.metadata.filesize = filesize2;
        info2.metadata.checksum = checksum2;
        info2.metadata.tgPublisher = publisher;
        infoList.add(info2);

        List<HandleInfo> pids = pidService.getTextgridPids(infoList, rbacSid);

        System.out.println(
            "\tnew pids for urls " + info1.metadata.url + ", " + info2.metadata.url + ": " + pids);

        // FIXME Check response!

        // assertTrue(pids != null && !pids.isEmpty());
        // assertTrue(pids.contains(url1) && pids.contains(url2));

        // TODO Check extended metadata!

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  /**
   * <p>
   * NOTE Must be tested with DH-crud, for the DH-rep PID Service is not publicly available!
   * </p>
   */
  @Test
  @Ignore
  public void testGetDariahDoi() {
    // @Path("/getDariahDoi")
  }

  /**
   * <p>
   * NOTE Must be tested with DH-crud, for the DH-rep PID Service is not publicly available!
   * </p>
   */
  @Test
  @Ignore
  public void testGetDariahPid() {
    // @Path("/getDariahPid")
  }

  /**
   * <p>
   * Test updating Handle metadata.
   * </p>
   * 
   * @throws PidServiceException
   * @throws HandleException
   */
  @Test
  public void testUpdateMetadata() throws PidServiceException, HandleException {

    System.out.println("testUpdateMetadata()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        // First create and check a new PID.
        String url = "https://textgridrep.org/junit";
        String suffix = UUID.randomUUID().toString();
        String filesize = "432!";
        String checksum = "md5:0958745024378";
        String publisher = "TextGrid";

        HandleInfo info = new HandleInfo(suffix, url);
        info.metadata.filesize = filesize;
        info.metadata.checksum = checksum;
        info.metadata.tgPublisher = publisher;

        String pid = getTextgridPid(info);

        checkTextgridPid(pid, url);

        // Then set PID and update metadata (adds only the NEW key/value pairs!).
        info.handle = pid;
        String newUrl = "https://textgridrep.org/junitADD2";
        info.metadata.url = newUrl;
        info.metadata.tgMetadata = newUrl + "/metadata";
        info.metadata.pubdate = "1970-12-10";

        System.out.println("\tmetadata to be updated");

        pidService.updateMetadata(info, pidSecret);

        // Check for updated metadata.
        System.out.println("\tcheck updated metadata");

        checkTextgridPid(pid, newUrl);

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  /**
   * @throws PidServiceException
   * @throws HandleException
   */
  @Test
  public void testUpdateTextgridMetadata() throws PidServiceException, HandleException {

    System.out.println("testUpdateTextGridMetadata()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        // First create and check a new PID.
        String url = "https://textgridrep.org/junit";
        String suffix = UUID.randomUUID().toString();
        String filesize = "584376";
        String checksum = "md5:3298524350765572";
        String publisher = "TextGrid";

        HandleInfo info = new HandleInfo(suffix, url);
        info.metadata.filesize = filesize;
        info.metadata.checksum = checksum;
        info.metadata.tgPublisher = publisher;

        String pid = getTextgridPid(info);

        checkTextgridPid(pid, url);

        // Then update the TextGrid metadata (updates existing metadata).
        HandleInfo update = new HandleInfo();
        update.handle = pid;
        String newUrl = "https://textgridrep.org/junit-jupdate";
        String dhResponsible = "must not be added!";
        update.metadata.url = newUrl;
        update.metadata.tgMetadata = newUrl + "/metadata";
        update.metadata.dhResponsible = dhResponsible;
        System.out.println("\tmetadata to be updated");
        System.out.println("\t" + update.metadata);

        pidService.updateMetadata(update, pidSecret);

        // Check for existing updated metadata.
        System.out.println("\tcheck updated metadata");

        checkTextgridPid(pid, newUrl);

        // Check for NEW metadata.
        String typeResponsible = pidService.getMetadata(pid, HandleMetadata.MDTYPE_DH_RESPONSIBLE);

        System.out.println("\tcheck new metadata");
        System.out.println("\t" + HandleMetadata.MDTYPE_DH_RESPONSIBLE + "\t"
            + (typeResponsible.isEmpty() ? "NOT EXISTING (update MUST NOT add new metadata!)"
                : typeResponsible));

        assertFalse(typeResponsible.contains(dhResponsible));

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  /**
   * <p>
   * Test adding metadata to an existing PID.
   * </p>
   * 
   * @throws PidServiceException
   * @throws HandleException
   */
  @Test
  public void testAddMetadata() throws PidServiceException, HandleException {

    System.out.println("testAddMetadata()");

    // No test on productive systems!
    if (PROPERTIES.contains("DEV") ||
        PROPERTIES.contains("TEST") ||
        PROPERTIES.contains("LOCAL")) {

      if (testMethodsWithAuthentication) {
        // First create and check a new PID.
        String url = "https://textgridrep.org/junit";
        String suffix = UUID.randomUUID().toString();
        String filesize = "1234";
        String checksum = "md5:1234567890";
        String publisher = "fuuuguuu";

        HandleInfo info = new HandleInfo(suffix, url);
        info.metadata.filesize = filesize;
        info.metadata.checksum = checksum;
        info.metadata.tgPublisher = publisher;

        String pid = getTextgridPid(info);

        checkTextgridPid(pid, url);

        // Then set PID and add metadata.
        HandleInfo added = new HandleInfo();
        added.handle = pid;
        String dhResponsible = "testitest";
        added.metadata.dhResponsible = dhResponsible;

        System.out.println("\tmetadata to be added for " + added.handle);
        System.out.println("\t" + added.metadata.getMetadataMap());

        pidService.addMetadata(added, pidSecret);

        // Check for old metadata.
        System.out.println("\tcheck old metadata after add");

        checkTextgridPid(pid, url);

        // Check for NEW metadata.
        String typeResponsible = pidService.getMetadata(pid, HandleMetadata.MDTYPE_DH_RESPONSIBLE);

        System.out.println("\tcheck new metadata");
        System.out.println("\t?\t" + HandleMetadata.MDTYPE_DH_RESPONSIBLE + "\t" + typeResponsible);

        assertTrue(typeResponsible.contains(dhResponsible));

      } else {
        System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      }

    } else {
      System.out.println("\t" + PidServiceUtils.NO_PROD_TESTING);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theEndpoint
   * @throws IOException
   */
  private static synchronized void initTgpid(final String theEndpoint) throws IOException {

    if (pidService == null) {
      if (theEndpoint == null || theEndpoint.isEmpty()) {
        throw new IOException("pid service endpoint is not set!");
      }

      pidService = RestClientBuilder.newBuilder()
          .property("force.urlconnection.http.conduit", true)
          .baseUri(URI.create(theEndpoint))
          .register(JacksonJsonProvider.class)
          .build(PidService.class);
    }
  }

  /**
   * <p>
   * Gets Handle metadata types URL, URL_METADATA, PUBDATE, and CREATOR and checks for existence and
   * syntax.
   * </p>
   * 
   * @param thePid
   * @param theUrl
   * @throws PidServiceException
   * @throws HandleException
   */
  private static void checkTextgridPid(final String thePid,
      final String theUrl)
      throws PidServiceException, HandleException {

    String requestedTypes[] = {HandleMetadata.MDTYPE_URL,
        HandleMetadata.MDTYPE_TG_METADATA,
        HandleMetadata.MDTYPE_PUBDATE,
        HandleMetadata.MDTYPE_CREATOR};

    HandleValue vals[] = PidServiceUtils.resolveAuthoritative(
        hdlResolver,
        thePid,
        requestedTypes);

    PidServiceUtils.printHandleValues(vals);

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_URL));
    String typeUrl = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_URL);
    assertTrue(typeUrl.contains(theUrl));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_TG_METADATA));
    String typeMetadata =
        PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_TG_METADATA);
    assertTrue(typeMetadata.contains(theUrl + "/metadata"));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_PUBDATE));
    String typePubdate = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_PUBDATE);
    assertTrue(typePubdate != null && !typePubdate.isEmpty());

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_CREATOR));
    String typeCreator = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_CREATOR);
    assertTrue(typeCreator != null && typeCreator.contains(pidService.version()));
  }

  /**
   * <p>
   * Gets a new extended PID from the PID Service. Sets also CHECKSUM and FILESIZE metadata.
   * </p>
   * 
   * @param info
   * @return
   * @throws PidServiceException
   */
  private static String getTextgridPid(final HandleInfo info)
      throws PidServiceException {

    HandleInfo pidInfo = pidService.getTextgridPid(
        info,
        rbacSid);

    System.out.println("\tnew pid for url " + pidInfo.metadata.url + ": " + pidInfo.handle);

    assertTrue(pidInfo.handle != null && !pidInfo.handle.isEmpty());

    return pidInfo.handle;
  }

  /**
   * <p>
   * Checks metadata types URL, URL_METADATA, and also PUBDATE, CREATOR, FILESIZE, CHECKSUM, and
   * PUBLISHER and checks for existence and syntax.
   * </p>
   * 
   * @param thePid
   * @param theUrl
   * @param theFilesize
   * @param theChecksum
   * @param thePublisher
   * @throws PidServiceException
   * @throws HandleException
   */
  private static void checkExtendedTextgridPid(final String thePid,
      final String theUrl,
      final String theFilesize,
      final String theChecksum,
      final String thePublisher)
      throws PidServiceException, HandleException {

    String requestedTypes[] = {HandleMetadata.MDTYPE_URL,
        HandleMetadata.MDTYPE_TG_METADATA,
        HandleMetadata.MDTYPE_PUBDATE,
        HandleMetadata.MDTYPE_CREATOR,
        HandleMetadata.MDTYPE_FILESIZE,
        HandleMetadata.MDTYPE_CHECKSUM,
        HandleMetadata.MDTYPE_TG_PUBLISHER};

    HandleValue vals[] =
        PidServiceUtils.resolveAuthoritative(
            hdlResolver,
            thePid,
            requestedTypes);

    PidServiceUtils.printHandleValues(vals);

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_URL));
    String typeUrl = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_URL);
    assertTrue(typeUrl.contains(theUrl));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_TG_METADATA));
    String typeMetadata =
        PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_TG_METADATA);
    assertTrue(typeMetadata.contains(theUrl + "/metadata"));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_PUBDATE));
    String typePubdate = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_PUBDATE);
    assertTrue(typePubdate != null && !typePubdate.isEmpty());

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_CREATOR));
    String typeCreator = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_CREATOR);
    assertTrue(typeCreator != null && typeCreator.contains(pidService.version()));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_FILESIZE));
    String typeFilesize = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_FILESIZE);
    assertTrue(typeFilesize != null && typeFilesize.contains(theFilesize));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_CHECKSUM));
    String typeChecksum = PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_CHECKSUM);
    assertTrue(typeChecksum != null && typeChecksum.contains(theChecksum));

    assertTrue(PidServiceUtils.checkForType(vals, HandleMetadata.MDTYPE_TG_PUBLISHER));
    String typePublisher =
        PidServiceUtils.getHandleTypeValue(vals, HandleMetadata.MDTYPE_TG_PUBLISHER);
    assertTrue(typePublisher != null && typePublisher.contains(thePublisher));
  }

}
