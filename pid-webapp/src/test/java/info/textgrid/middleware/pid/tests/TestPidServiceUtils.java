/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import info.textgrid.middleware.pid.PidServiceUtils;
import info.textgrid.middleware.pid.api.HandleInfo;
import info.textgrid.middleware.pid.api.HandleMetadata;
import info.textgrid.middleware.pid.api.PidServiceException;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleValue;


/**
 * 
 */
public class TestPidServiceUtils {

  // **
  // FINALS
  // **

  public static final String DEFAULT_SUFFIX_TEST_VALUE = "1234.5";
  public static final String DEFAULT_URL_TEST_VALUE = "https://textgridrep.org";
  public static final String DEFAULT_METADATA_TEST_VALUE =
      DEFAULT_URL_TEST_VALUE + PidServiceUtils.METADATA_PATH;
  public static final String DEFAULT_FILESIZE_TEST_VALUE = "12345";
  public static final String DEFAULT_CHECKSUM_TEST_VALUE = "md5:5d576cbd5b365491ea8357b0e250bfec";
  public static final String DEFAULT_CREATOR_TEST_VALUE = "fugu";
  public static final String DEFAULT_PUBLISHER_TEST_VALUE = "TextGrid";

  /**
   * @throws PidServiceException
   */
  @Test
  public void testGetAddedHandleValues()
      throws PidServiceException {

    // Create expected metadata.
    HandleValue filesize =
        new HandleValue(5, HandleMetadata.MDTYPE_FILESIZE, DEFAULT_FILESIZE_TEST_VALUE);
    HandleValue expectedMetadata[] = {filesize};

    System.out.println("expected");
    PidServiceUtils.printHandleValues(expectedMetadata);

    // Create existing metadata.
    HandleValue url =
        new HandleValue(1, HandleMetadata.MDTYPE_URL, DEFAULT_URL_TEST_VALUE);
    HandleValue metadata = new HandleValue(2, HandleMetadata.MDTYPE_TG_METADATA,
        DEFAULT_URL_TEST_VALUE + PidServiceUtils.METADATA_PATH);
    HandleValue creator =
        new HandleValue(3, HandleMetadata.MDTYPE_CREATOR, DEFAULT_PUBLISHER_TEST_VALUE);
    HandleValue checksum =
        new HandleValue(4, HandleMetadata.MDTYPE_CHECKSUM, DEFAULT_CHECKSUM_TEST_VALUE);
    HandleValue existingMetadata[] = {url, metadata, creator, checksum};

    System.out.println("existing");
    PidServiceUtils.printHandleValues(existingMetadata);

    // Create HandleInfo key value pairs to add.
    HandleInfo info = new HandleInfo(DEFAULT_SUFFIX_TEST_VALUE, DEFAULT_URL_TEST_VALUE);
    info.metadata.filesize = DEFAULT_FILESIZE_TEST_VALUE;

    System.out.println("metadata added: " + info.metadata);

    HandleValue values[] = PidServiceUtils.getAddedHandleValues(existingMetadata, info);

    System.out.println("response [" + values.length + "]");
    PidServiceUtils.printHandleValues(values);

    assertTrue(HandleValue.unorderedEqualsIgnoreTimestamp(expectedMetadata, values));
  }

  /**
   * @throws PidServiceException
   */
  @Test
  public void testGetUpdatedHandleValues()
      throws PidServiceException {

    // Create expected metadata.
    HandleValue url =
        new HandleValue(1, HandleMetadata.MDTYPE_URL, DEFAULT_URL_TEST_VALUE + "/modified");
    HandleValue metadata = new HandleValue(2, HandleMetadata.MDTYPE_TG_METADATA,
        DEFAULT_URL_TEST_VALUE + "/modified" + PidServiceUtils.METADATA_PATH);
    HandleValue creator =
        new HandleValue(3, HandleMetadata.MDTYPE_CREATOR, DEFAULT_CREATOR_TEST_VALUE);
    HandleValue checksum =
        new HandleValue(4, HandleMetadata.MDTYPE_CHECKSUM, DEFAULT_CHECKSUM_TEST_VALUE);
    HandleValue expectedMetadata[] = {url, metadata, creator, checksum};

    System.out.println("expected");
    PidServiceUtils.printHandleValues(expectedMetadata);

    // Create existing metadata.
    url = new HandleValue(1, HandleMetadata.MDTYPE_URL, DEFAULT_URL_TEST_VALUE);
    metadata = new HandleValue(2, HandleMetadata.MDTYPE_TG_METADATA,
        DEFAULT_URL_TEST_VALUE + PidServiceUtils.METADATA_PATH);
    creator = new HandleValue(3, HandleMetadata.MDTYPE_CREATOR, DEFAULT_CREATOR_TEST_VALUE);
    checksum = new HandleValue(4, HandleMetadata.MDTYPE_CHECKSUM, DEFAULT_CHECKSUM_TEST_VALUE);
    HandleValue existingMetadata[] = {url, metadata, creator, checksum};

    System.out.println("existing");
    PidServiceUtils.printHandleValues(existingMetadata);

    // Create key value pairs to update.
    HandleInfo info =
        new HandleInfo(DEFAULT_SUFFIX_TEST_VALUE, DEFAULT_URL_TEST_VALUE + "/modified");
    info.metadata.tgMetadata = DEFAULT_URL_TEST_VALUE + "/modified/metadata";
    info.metadata.filesize = DEFAULT_FILESIZE_TEST_VALUE;

    System.out.println("update metadata: " + info.metadata);

    HandleValue values[] = PidServiceUtils.getUpdatedHandleValues(existingMetadata, info);

    System.out.println("response");
    PidServiceUtils.printHandleValues(values);

    assertTrue(HandleValue.unorderedEqualsIgnoreTimestamp(expectedMetadata, values));
  }

  /**
   * 
   */
  @Test
  public void testNewTGPidErrorException() {

    String message = "HIER LÄUFT WAS FALSCH!";
    IOException i = new IOException(message);
    PidServiceException e = PidServiceUtils.newTGPidError(i);

    assertTrue(e.getMessage().contains(i.getMessage()));
  }

  /**
   * 
   */
  @Test
  public void testNewTGPidErrorMessage() {

    String message = "HIER LÄUFT WAS FALSCH!";
    PidServiceException e = PidServiceUtils.newTGPidError(message);

    assertTrue(message.equals(e.getMessage()));
  }

  /**
   * @throws HandleException
   */
  @Test
  public void testConvertToBytes()
      throws HandleException {

    String stypes[] = {"fugu", "blubb", "krams", "testitest"};
    byte conversion[][] = PidServiceUtils.convert2Bytes(stypes);

    // Convert both to string list.
    List<String> stringsFromSource = new ArrayList<String>();
    for (String s : stypes) {
      stringsFromSource.add(s);
    }
    List<String> stringsFromConversion = new ArrayList<String>();
    for (byte c[] : conversion) {
      stringsFromConversion.add(new String(c));
    }

    assertTrue(stringsFromSource.equals(stringsFromConversion));
  }

  /**
   * 
   */
  @Test
  public void testConvertCsv2ArrayNoSpaces() {

    String csv = "krams,dingsbums,testitest";
    String expectedArray[] = {"krams", "dingsbums", "testitest"};
    String csvValues[] = PidServiceUtils.convertCsv2Array(csv);

    for (int i = 0; i < expectedArray.length; i++) {
      assertTrue(expectedArray[i].equals(csvValues[i]));
    }
  }

  /**
   * 
   */
  @Test
  public void testConvertCsv2ArrayWithSpaces() {

    String csv = "krams, dingsbums, testitest";
    String expectedArray[] = {"krams", "dingsbums", "testitest"};
    String csvValues[] = PidServiceUtils.convertCsv2Array(csv);

    for (int i = 0; i < expectedArray.length; i++) {
      assertTrue(expectedArray[i].equals(csvValues[i]));
    }
  }

  /**
   * 
   */
  @Test
  public void testConvertCsv2ArrayEmpty() {

    String csv = "";
    String expectedArray[] = {""};
    String csvValues[] = PidServiceUtils.convertCsv2Array(csv);

    for (int i = 0; i < expectedArray.length; i++) {
      assertTrue(expectedArray[i].equals(csvValues[i]));
    }
  }

  /**
   * 
   */
  @Test
  public void testConvertCsv2ArrayNull() {

    String csv = null;
    String expectedArray[] = {""};
    String csvValues[] = PidServiceUtils.convertCsv2Array(csv);

    for (int i = 0; i < expectedArray.length; i++) {
      assertTrue(expectedArray[i].equals(csvValues[i]));
    }
  }

}
