/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import net.handle.hdllib.HandleException;
import net.handle.hdllib.ScanCallback;

/**
 * <p>
 * List requests are not used in PID Service at the moment.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
public class SimpleListCallback implements ScanCallback {

  /**
   *
   */
  @Override
  public void scanHandle(byte[] handle) throws HandleException {
    System.out.println(handle);
  }

}
