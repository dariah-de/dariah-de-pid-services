/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Properties;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.pid.doi.DataCiteDOIUtil;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * 
 */
public class TestDariahDoi {

  private static final String DOI_OBJ =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"a Contributor\" ;\n"
          + "        dc:coverage     \"a Coverage\" ;\n"
          + "        dc:creator      \"a Creator\" ;\n"
          + "        dc:date         \"2017-06-13\" ;\n"
          + "        dc:description  \"a Description\" ;\n"
          + "        dc:format       \"a Format\" ;\n"
          + "        dc:identifier   \"an Identifier\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/an/identifier> ;\n"
          + "        dc:language     \"de-DE\" ;\n" + "        dc:publisher    \"a Publicator\" ;\n"
          + "        dc:relation     \"a Relation\" ;\n"
          + "        dc:rights       \"a Rightholder\" ;\n"
          + "        dc:source       \"a Source\" ;\n" + "        dc:subject      \"a Topic\" ;\n"
          + "        dc:title        \"a Title\" ;\n" + "        dc:type         \"a Type\" .\n";
  private static final String DOI_OBJ_DATAOBJECT =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.11113/0000-000B-C8F0-4>\n"
          + "        a               dariah:DataObject ;\n"
          + "        dc:creator      \"DARIAH-DE\" ;\n"
          + "        dc:description  \"The DARIAH-DE Repository terms of use (English)\" ;\n"
          + "        dc:format       \"text/html\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.11113/0000-000B-C8F0-4> ;\n"
          + "        dc:language     \"en\" ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.11113/0000-000B-C8EF-7> ;\n"
          + "        dc:rights       \"https://creativecommons.org/choose/zero/\" , \"CC0\" ;\n"
          + "        dc:title        \"DARIAH-DE Repository – Terms of Use\" .\n";
  private static final String EXPECTED_DOI_OBJ_DATAOBJECT =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.20375/0000-000B-C8F0-4</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>DARIAH-DE</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n"
          + "        <title>DARIAH-DE Repository – Terms of Use</title>\n" + "    </titles>\n"
          + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Dataset\">Dataset</resourceType>\n"
          + "    <language>en</language>\n" + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.11113/0000-000B-C8F0-4</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.11113/0000-000B-C8EF-7</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n"
          + "        <format>text/html</format>\n" + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>https://creativecommons.org/choose/zero/</rights>\n"
          + "        <rights>CC0</rights>\n" + "    </rightsList>\n" + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">The DARIAH-DE Repository terms of use (English)</description>\n"
          + "    </descriptions>\n" + "</resource>\n";
  private static final String DOI_OBJ_COLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.11113/0000-000B-C8EF-7>\n"
          + "        a               dariah:Collection ;\n"
          + "        dc:creator      \"DARIAH-DE\" ;\n"
          + "        dc:description  \"The collection containing the terms of use for the DARIAH-DE Repository.\" ;\n"
          + "        dc:format       \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.11113/0000-000B-C8EF-7> ;\n"
          + "        dc:rights       \"CC0\" , \"https://creativecommons.org/choose/zero/\" ;\n"
          + "        dc:title        \"DARIAH-DE Repository – Terms of Use\" .\n";
  private static final String EXPECTED_DOI_OBJ_COLLECTION =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.20375/0000-000B-C8EF-7</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>DARIAH-DE</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n"
          + "        <title>DARIAH-DE Repository – Terms of Use</title>\n" + "    </titles>\n"
          + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Collection\">Collection</resourceType>\n"
          + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.11113/0000-000B-C8EF-7</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n"
          + "        <format>text/vnd.dariah.dhrep.collection+turtle</format>\n"
          + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>https://creativecommons.org/choose/zero/</rights>\n"
          + "        <rights>CC0</rights>\n" + "    </rightsList>\n" + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">The collection containing the terms of use for the DARIAH-DE Repository.</description>\n"
          + "    </descriptions>\n" + "</resource>\n";
  private static final String EXPECTED_DOI_OBJ =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/urgl-argl-aua</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>a Creator</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n" + "        <title>a Title</title>\n"
          + "    </titles>\n" + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Dataset\">a Type</resourceType>\n"
          + "    <subjects>\n" + "        <subject>a Topic</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>a Contributor</contributorName>\n"
          + "        </contributor>\n" + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2017-06-13</date>\n" + "    </dates>\n"
          + "    <language>de-DE</language>\n" + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.1113/urgl-argl-aua</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">a Relation</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">a Source</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n"
          + "        <format>a Format</format>\n" + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>a Rightholder</rights>\n" + "    </rightsList>\n"
          + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">a Description</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">a Coverage</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>\n";
  private static final String DOI_OBJ_WITH_UMLAUTS =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"a Contribütor\" ;\n"
          + "        dc:coverage     \"a Cöverage\" ;\n"
          + "        dc:creator      \"a Creator\" ;\n"
          + "        dc:date         \"2017-06-13\" ;\n"
          + "        dc:description  \"a Descriptiön\" ;\n"
          + "        dc:format       \"a Format\" ;\n"
          + "        dc:identifier   \"an Identifiér\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/an/Identifier> ;\n"
          + "        dc:language     \"de-DE\" ;\n" + "        dc:publisher    \"a Publicator\" ;\n"
          + "        dc:relation     \"a Relation\" ;\n"
          + "        dc:rights       \"a Righthølder\" ;\n"
          + "        dc:source       \"a Source\" ;\n" + "        dc:subject      \"a Topic\" ;\n"
          + "        dc:title        \"a Title\" ;\n" + "        dc:type         \"a Type\" .\n";
  private static final String EXPECTED_DOI_OBJ_WITH_UMLAUTS =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/urgl-argl-aua</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>a Creator</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n" + "        <title>a Title</title>\n"
          + "    </titles>\n" + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Dataset\">a Type</resourceType>\n"
          + "    <subjects>\n" + "        <subject>a Topic</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>a Contribütor</contributorName>\n"
          + "        </contributor>\n" + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2017-06-13</date>\n" + "    </dates>\n"
          + "    <language>de-DE</language>\n" + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.1113/urgl-argl-aua</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">a Relation</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">a Source</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n"
          + "        <format>a Format</format>\n" + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>a Righthølder</rights>\n" + "    </rightsList>\n"
          + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">a Descriptiön</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">a Cöverage</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>\n";
  private static final String DOI_OBJ_WITH_SONDERZEICHENS =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"\\\")($Q\\\"/$Q=\\\")LKDSJ\" ;\n"
          + "        dc:coverage     \"å‚ƒœæ‚åƒ∂‚∂\" ;\n"
          + "        dc:creator      \"!:§!$∂‚ƒç≈√∂Ω†®†'¶¢[¿“¶[¢\" ;\n"
          + "        dc:date         \"2018-02-19T11:09:00Z\" ;\n"
          + "        dc:description  \"€‚∂ƒ‚∂©∂@ƒ∂ƒ©\" ;\n"
          + "        dc:format       \"‚ƒ∂‚æ∂ƒ\" ;\n" + "        dc:identifier   \"∑€[¶¢'\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.T11991/0000-0008-B0BC-D> ;\n"
          + "        dc:language     \"de\" ;\n" + "        dc:publisher    \"‚†•π®€≈ç√æ\" ;\n"
          + "        dc:relation     \"“'¢•‚å‘∂œæ≈∞ç√\" ;\n"
          + "        dc:rights       \"“¶['“¢}[“¶•¢ø\" ;\n"
          + "        dc:source       \"ÍÆÏŒÏÍŒ„°¸∏Ø\" ;\n"
          + "        dc:subject      \"ÆÏ˛Œ™#°#Ø\" ;\n"
          + "        dc:title        \"Sonderzeichen (\\\"$&?=!§!§\" ;\n"
          + "        dc:type         \"Æ™Í„∏¸\" .\n";
  private static final String EXPECTED_DOI_OBJ_WITH_SONDERZEICHENS =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/0000-0008-B0BC-D</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>!:§!$∂‚ƒç≈√∂Ω†®†'¶¢[¿“¶[¢</creatorName>\n"
          + "        </creator>\n" + "    </creators>\n" + "    <titles>\n"
          + "        <title>Sonderzeichen (\"$&amp;?=!§!§</title>\n" + "    </titles>\n"
          + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Dataset\">Æ™Í„∏¸</resourceType>\n"
          + "    <subjects>\n" + "        <subject>ÆÏ˛Œ™#°#Ø</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>\")($Q\"/$Q=\")LKDSJ</contributorName>\n"
          + "        </contributor>\n" + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2018-02-19T11:09:00Z</date>\n" + "    </dates>\n"
          + "    <language>de</language>\n" + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.T11991/0000-0008-B0BC-D</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">“'¢•‚å‘∂œæ≈∞ç√</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">ÍÆÏŒÏÍŒ„°¸∏Ø</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n" + "        <format>‚ƒ∂‚æ∂ƒ</format>\n"
          + "    </formats>\n" + "    <rightsList>\n" + "        <rights>“¶['“¢}[“¶•¢ø</rights>\n"
          + "    </rightsList>\n" + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">€‚∂ƒ‚∂©∂@ƒ∂ƒ©</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">å‚ƒœæ‚åƒ∂‚∂</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>\n";
  private static final String DOI_OBJ_WITHOUT_LANGUAGE_TAG =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"Der fugu\" ;\n" + "        dc:coverage     \"schubidu\" ;\n"
          + "        dc:creator      \"fugu\" ;\n"
          + "        dc:date         \"2018-02-19T11:09:00Z\" ;\n"
          + "        dc:description  \"Hier ist kein LANGUAGE-Tag drin!\" ;\n"
          + "        dc:format       \"Dingsda\" ;\n"
          + "        dc:identifier   \"fugufugufugu\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.T11991/0000-0008-B0BC-D> ;\n"
          + "        dc:publisher    \"fugu\" ;\n" + "        dc:relation     \"keine zu nix\" ;\n"
          + "        dc:rights       \"Dies gehört dem fugu\" ;\n"
          + "        dc:source       \"Ein Testfall aus einem Testfall\" ;\n"
          + "        dc:subject      \"fugu\" ;\n"
          + "        dc:title        \"fugus neuer Testfall\" ;\n"
          + "        dc:type         \"Text\" .\n";
  private static final String EXPECTED_DOI_OBJ_WITHOUT_LANGUAGE_TAG =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/0000-0008-B0BC-D</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>fugu</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n" + "        <title>fugus neuer Testfall</title>\n"
          + "    </titles>\n" + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Text\">Text</resourceType>\n"
          + "    <subjects>\n" + "        <subject>fugu</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>Der fugu</contributorName>\n" + "        </contributor>\n"
          + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2018-02-19T11:09:00Z</date>\n" + "    </dates>\n"
          + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.T11991/0000-0008-B0BC-D</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">keine zu nix</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">Ein Testfall aus einem Testfall</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n" + "        <format>Dingsda</format>\n"
          + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>Dies gehört dem fugu</rights>\n" + "    </rightsList>\n"
          + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">Hier ist kein LANGUAGE-Tag drin!</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">schubidu</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>\n";
  private static final String DOI_OBJ_WITH_EMPTY_LANGUAGE_TAG =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"Der fugu\" ;\n" + "        dc:coverage     \"schubidu\" ;\n"
          + "        dc:creator      \"fugu\" ;\n"
          + "        dc:date         \"2018-02-19T11:09:00Z\" ;\n"
          + "        dc:description  \"Hier ist ein LEERER LANGUAGE-Tag drin!\" ;\n"
          + "        dc:format       \"Dingsda\" ;\n"
          + "        dc:identifier   \"fugufugufugu\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.T11991/0000-0008-B0BC-D> ;\n"
          + "        dc:language     \"\" ;\n" + "        dc:publisher    \"fugu\" ;\n"
          + "        dc:relation     \"keine zu nix\" ;\n"
          + "        dc:rights       \"Dies gehört dem fugu\" ;\n"
          + "        dc:source       \"Ein Testfall aus einem Testfall\" ;\n"
          + "        dc:subject      \"fugu\" ;\n"
          + "        dc:title        \"fugus neuer Testfall\" ;\n"
          + "        dc:type         \"Text\" .\n";
  private static final String EXPECTED_DOI_OBJ_WITH_EMPTY_LANGUAGE_TAG =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/0000-0008-B0BC-D</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>fugu</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n" + "        <title>fugus neuer Testfall</title>\n"
          + "    </titles>\n" + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Text\">Text</resourceType>\n"
          + "    <subjects>\n" + "        <subject>fugu</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>Der fugu</contributorName>\n" + "        </contributor>\n"
          + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2018-02-19T11:09:00Z</date>\n" + "    </dates>\n"
          + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.T11991/0000-0008-B0BC-D</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">keine zu nix</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">Ein Testfall aus einem Testfall</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n" + "        <format>Dingsda</format>\n"
          + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>Dies gehört dem fugu</rights>\n" + "    </rightsList>\n"
          + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">Hier ist ein LEERER LANGUAGE-Tag drin!</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">schubidu</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>";
  private static final String DOI_OBJ_WITH_WRONG_LANGUAGE_TAG =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:479961  a  dariah:Collection ;\n"
          + "        dc:contributor  \"Der fugu\" ;\n" + "        dc:coverage     \"schubidu\" ;\n"
          + "        dc:creator      \"fugu\" ;\n"
          + "        dc:date         \"2018-02-19T11:09:00Z\" ;\n"
          + "        dc:description  \"Hier ist ein LEERER LANGUAGE-Tag drin!\" ;\n"
          + "        dc:format       \"Dingsda\" ;\n"
          + "        dc:identifier   \"fugufugufugu\" ;\n"
          + "        dc:identifier   <http://hdl.handle.net/21.T11991/0000-0008-B0BC-D> ;\n"
          + "        dc:language     \"THISISTOTALLYWRONGLANGUAGETAG!!! MUST BE '([a-zA-Z]{2}|[iI]-[a-zA-Z]+|[xX]-[a-zA-Z]{1,8})(-[a-zA-Z]{1,8})*'\" ;\n"
          + "        dc:publisher    \"fugu\" ;\n" + "        dc:relation     \"keine zu nix\" ;\n"
          + "        dc:rights       \"Dies gehört dem fugu\" ;\n"
          + "        dc:source       \"Ein Testfall aus einem Testfall\" ;\n"
          + "        dc:subject      \"fugu\" ;\n"
          + "        dc:title        \"fugus neuer Testfall\" ;\n"
          + "        dc:type         \"Text\" .\n";
  private static final String EXPECTED_DOI_OBJ_WITH_WRONG_LANGUAGE_TAG =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          + "    <identifier identifierType=\"DOI\">10.abcde/0000-0008-B0BC-D</identifier>\n"
          + "    <creators>\n" + "        <creator>\n"
          + "            <creatorName>fugu</creatorName>\n" + "        </creator>\n"
          + "    </creators>\n" + "    <titles>\n" + "        <title>fugus neuer Testfall</title>\n"
          + "    </titles>\n" + "    <publisher>DARIAH-DE</publisher>\n" + "    <publicationYear>"
          + Calendar.getInstance().get(Calendar.YEAR) + "</publicationYear>\n"
          + "    <resourceType resourceTypeGeneral=\"Text\">Text</resourceType>\n"
          + "    <subjects>\n" + "        <subject>fugu</subject>\n" + "    </subjects>\n"
          + "    <contributors>\n" + "        <contributor contributorType=\"Other\">\n"
          + "            <contributorName>Der fugu</contributorName>\n" + "        </contributor>\n"
          + "    </contributors>\n" + "    <dates>\n"
          + "        <date dateType=\"Created\">2018-02-19T11:09:00Z</date>\n" + "    </dates>\n"
          + "    <relatedIdentifiers>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.T11991/0000-0008-B0BC-D</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"HasPart\">keine zu nix</relatedIdentifier>\n"
          + "        <relatedIdentifier relatedIdentifierType=\"URN\" relationType=\"IsDerivedFrom\">Ein Testfall aus einem Testfall</relatedIdentifier>\n"
          + "    </relatedIdentifiers>\n" + "    <formats>\n" + "        <format>Dingsda</format>\n"
          + "    </formats>\n" + "    <rightsList>\n"
          + "        <rights>Dies gehört dem fugu</rights>\n" + "    </rightsList>\n"
          + "    <descriptions>\n"
          + "        <description descriptionType=\"Other\">Hier ist ein LEERER LANGUAGE-Tag drin!</description>\n"
          + "    </descriptions>\n" + "    <geoLocations>\n" + "        <geoLocation>\n"
          + "            <geoLocationPlace xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xs:string\">schubidu</geoLocationPlace>\n"
          + "        </geoLocation>\n" + "    </geoLocations>\n" + "</resource>";
  private static final String EXPECTED = "EXPECTED\n";
  private static final String CREATED = "CREATED\n";

  private String testServer;
  private String auth;

  /*
   * NOTE: Please set doi.targetUrl in pid.properties to http://dhd-blog.org or hdl.handle.net or
   * repository.de.dariah.eu else it will fail because other URLs are not allowed yet from DataCite.
   */

  /**
   * 
   */
  @Test
  public void testGetDOIIdentifier() {

    String doiPrefix = "10.abcde";
    String hdl = "21.1113/urgl-argl-aua";
    String expectedDoi = doiPrefix + "/urgl-argl-aua";

    String doi = DataCiteDOIUtil.getDOIIdentifier(hdl, doiPrefix);

    if (!doi.equals(expectedDoi)) {
      System.out.println("HDL: " + hdl);
      System.out.println("DOI: " + doi + " ≠ " + expectedDoi);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithoutUmlauts() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ,
        "21.1113/urgl-argl-aua", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLCollectionType() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_COLLECTION,
        "21.11113/0000-000B-C8EF-7", true, "10.20375", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_COLLECTION.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_COLLECTION);
      System.out.println();
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLDataObjectType() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_DATAOBJECT,
        "21.11113/0000-000B-C8F0-4", true, "10.20375", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_DATAOBJECT.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_DATAOBJECT);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithSonderzeichens() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_WITH_SONDERZEICHENS,
        "21.T11991/0000-0008-B0BC-D", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_WITH_SONDERZEICHENS.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_WITH_SONDERZEICHENS);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithEmptyLanguageTag() throws Exception {

    String doiMetadata =
        DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_WITH_EMPTY_LANGUAGE_TAG,
            "21.T11991/0000-0008-B0BC-D", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_WITH_EMPTY_LANGUAGE_TAG.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_WITH_EMPTY_LANGUAGE_TAG);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithWrongLanguageTag() throws Exception {

    String doiMetadata =
        DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_WITH_WRONG_LANGUAGE_TAG,
            "21.T11991/0000-0008-B0BC-D", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_WITH_WRONG_LANGUAGE_TAG.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_WITH_WRONG_LANGUAGE_TAG);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithoutLanguageTag() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_WITHOUT_LANGUAGE_TAG,
        "21.T11991/0000-0008-B0BC-D", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_WITHOUT_LANGUAGE_TAG.trim().equals(doiMetadata.trim())) {
      System.out.println(EXPECTED + EXPECTED_DOI_OBJ_WITHOUT_LANGUAGE_TAG);
      System.out.println(CREATED + doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * @throws Exception
   */
  @Test
  public void testConvertMetadataToDataciteXMLWithUmlauts() throws Exception {

    String doiMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(DOI_OBJ_WITH_UMLAUTS,
        "21.1113/urgl-argl-aua", true, "10.abcde", "DARIAH-DE");

    if (!EXPECTED_DOI_OBJ_WITH_UMLAUTS.trim().equals(doiMetadata.trim())) {
      System.out.println(doiMetadata);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * PLEASE NOTE: ONLINE TEST, SHOULD NORMALLY BE IGNORED!
   * </p>
   * 
   * @throws IOException
   */
  @Test
  @Ignore
  public void testCreatingDOIWithoutUmlauts() throws IOException {

    // read our test configuration
    Properties prop = new Properties();

    InputStream inputStream =
        TestDariahDoi.class.getClassLoader().getResourceAsStream("test.properties");

    if (inputStream != null) {
      prop.load(inputStream);
    } else {
      throw new FileNotFoundException("property file test.properties not found in the classpath");
    }

    this.testServer = prop.getProperty("test.server");
    this.auth = prop.getProperty("test.auth");

    System.out.println("server: " + this.testServer);
    System.out.println("auth: " + this.auth);

    // lets see if the REST service is reachable at all, if not we can skip
    // all tests ....
    HttpURLConnection connection = null;
    try {
      URL url = new URL(this.testServer + "/version");
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      int code = connection.getResponseCode();

      System.out.println("version: " + IOUtils.readStringFromStream(connection.getInputStream()));

      assertEquals("Cannot reach server", 200, code);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }

    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(this.testServer).path("getDariahDoi");

    // Add key-value pair into the form object
    Form form = new Form();
    form.param("pid", "urgl-argl-aua");
    form.param("doimeta", DOI_OBJ);
    form.param("auth", this.auth);

    // Send the form object along with the post call
    Response response =
        webTarget.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
    System.out.println("Response code: " + response.getStatus());
    System.out.println("Response value: " + response.readEntity(String.class));

    assertEquals(response.readEntity(String.class), 200, response.getStatus());
  }

  /**
   * <p>
   * PLEASE NOTE: ONLINE TEST, SHOULD NORMALLY BE IGNORED!
   * </p>
   * 
   * @throws IOException
   */
  @Test
  @Ignore
  public void testCreatingDOIWithUmlauts() throws IOException {

    // read our test configuration
    Properties prop = new Properties();

    InputStream inputStream =
        TestDariahDoi.class.getClassLoader().getResourceAsStream("test.properties");

    if (inputStream != null) {
      prop.load(inputStream);
    } else {
      throw new FileNotFoundException("property file test.properties not found in the classpath");
    }

    this.testServer = prop.getProperty("test.server");
    this.auth = prop.getProperty("test.auth");

    System.out.println("server: " + this.testServer);
    System.out.println("auth: " + this.auth);

    // lets see if the REST service is reachable at all, if not we can skip
    // all tests ....
    HttpURLConnection connection = null;
    try {
      URL url = new URL(this.testServer + "/version");
      connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      int code = connection.getResponseCode();

      System.out.println("version: " + IOUtils.readStringFromStream(connection.getInputStream()));

      assertEquals("Cannot reach server", 200, code);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (connection != null) {
        connection.disconnect();
      }
    }

    Client client = ClientBuilder.newClient();
    WebTarget webTarget = client.target(this.testServer).path("getDariahDoi");

    // Add key-value pair into the form object
    Form form = new Form();
    form.param("pid", "urgl-argl-aua");
    form.param("doimeta", DOI_OBJ_WITH_UMLAUTS);
    form.param("auth", this.auth);

    System.out.println(DOI_OBJ_WITH_UMLAUTS);

    // Send the form object along with the post call
    Response response =
        webTarget.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));

    System.out.println("Response code: " + response.getStatus());
    System.out.println("Response value: " + response.readEntity(String.class));

    assertEquals(response.readEntity(String.class), 200, response.getStatus());
  }

}
