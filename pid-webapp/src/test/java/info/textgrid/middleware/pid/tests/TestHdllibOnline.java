/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.tests;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.middleware.pid.PidServiceUtils;
import info.textgrid.middleware.pid.api.HandleMetadata;
import net.handle.hdllib.AuthenticationInfo;
import net.handle.hdllib.Configuration;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.SecretKeyAuthenticationInfo;
import net.handle.hdllib.Util;

/**
 * <p>
 * This is a test class for testing the hdllib functionalities. Needs accurate properties file for
 * complete test run.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class TestHdllibOnline {

  // **
  // FINAL
  // **

  private static final String DEFAULT_PROPERTIES = "./src/test/resources/test.properties";
  // If the PROPERTIES file is not found, DEFAULT_PROPERTIES file is being used, no methods with
  // authentication are tested then!
  private static final String PROPERTIES = "./src/test/resources/DEV.test.properties";
  private static final String TYPE_TEST = "TEST";

  // **
  // STATIC
  // **

  private static HandleResolver hdlResolver = new HandleResolver();
  // Set auth testing default to true.
  private static boolean testMethodsWithAuthentication = true;
  private static String hsPrefix;
  private static String hsAdminUser;
  private static String hsAaminPass;
  private static int hsAdminIndex;
  private static AuthenticationInfo authInfo;

  /**
   * <p>
   * Set up config and auth.
   * </p>
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    Configuration serviceConf = hdlResolver.getConfiguration();

    System.out.println("primary site info: " + Util.getPrimarySite(serviceConf.getGlobalSites()));

    // Load properties file and properties.
    File conf = new File(PROPERTIES);

    System.out.print("look for config file " + conf.getName() + "...");

    if (!conf.exists()) {
      conf = new File(DEFAULT_PROPERTIES);

      System.out.println("not found");
      System.out.println("use default config file " + conf.getName());

      // Do not test methods that need authentication if default config file is used! There are no
      // auth values!
      testMethodsWithAuthentication = false;

      System.out.println("auth method testing disabled");

    } else {
      System.out.println("found");
    }

    System.out.println("load config");

    Properties p = new Properties();
    p.load(new FileInputStream(conf));

    hsPrefix = p.getProperty("HS_PREFIX");
    hsAdminUser = p.getProperty("HS_ADMIN_USER");
    hsAdminIndex = Integer.valueOf(p.getProperty("HS_ADMIN_INDEX"));
    hsAaminPass = p.getProperty("HS_ADMIN_PASS");

    // Create auth info with username and password.
    authInfo = new SecretKeyAuthenticationInfo(
        Util.encodeString(hsPrefix + "/" + hsAdminUser),
        hsAdminIndex,
        Util.encodeString(hsAaminPass),
        true);

    System.out.println("set up auth info\n" +
        "\tauth type: " + Util.decodeString(authInfo.getAuthType()) + "\n" +
        "\tauth info: " + authInfo.toString());
  }

  /**
   * <p>
   * Test resolving a Handle PID. Create a PID first. Gets all metadata, and looks for URL,
   * CHECKSUM, and PUBDATE metadata and correct values.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testResolveHandleAllMetadata() throws HandleException, IOException {

    System.out.println("testResolveHandleAllMetadata()");

    if (testMethodsWithAuthentication) {

      System.out.println("\t" + PidServiceUtils.CREATING_PID_MESSAGE);

      // Create PID for resolving tests with default metadata types.
      HandleValue inVals[] =
          createDefaultHandleValues(TestPidServiceUtils.DEFAULT_URL_TEST_VALUE, authInfo);
      String pid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      // Resolve PID.
      System.out.println("\tresolve pid " + pid);

      HandleValue outVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(outVals);

      // Check values.
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, pid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test resolving a Handle PID. Create a PID first. Gets and looks for URL, CHECKSUM, and PUBDATE
   * metadata and correct values. Uses authoritative query for time critical queries.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testResolveAuthotitativeHandle() throws HandleException, IOException {

    System.out.println("testResolveAuthotitativeHandle()");

    if (testMethodsWithAuthentication) {

      System.out.println("\t" + PidServiceUtils.CREATING_PID_MESSAGE);

      // Create PID for resolving tests with default metadata types.
      HandleValue inVals[] =
          createDefaultHandleValues(TestPidServiceUtils.DEFAULT_URL_TEST_VALUE, authInfo);
      String pid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      // Resolve PID.
      System.out.println("\tresolve pid " + pid + " (authoritative)");

      String requestedTypes[] = {HandleMetadata.MDTYPE_URL, HandleMetadata.MDTYPE_CHECKSUM,
          HandleMetadata.MDTYPE_PUBDATE};

      HandleValue outVals[] =
          PidServiceUtils.resolveAuthoritative(hdlResolver, pid, requestedTypes);

      PidServiceUtils.printHandleValues(outVals);

      // Check values.
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, pid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test modifying handle metadata values. Create a PID first, and modify the returned handle
   * values for updates.
   * 
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testModifyExistingHandleMetadataWithResolvingFirst()
      throws HandleException, IOException {

    System.out.println("testModifyHandleMetadataWithResolvingFirst()");

    if (testMethodsWithAuthentication) {

      System.out.println("\t" + PidServiceUtils.CREATING_PID_MESSAGE);

      // Create PID.
      HandleValue inVals[] =
          createDefaultHandleValues(TestPidServiceUtils.DEFAULT_URL_TEST_VALUE, authInfo);
      String pid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      // Resolve PID.
      System.out.println("\tresolve pid " + pid);

      HandleValue outVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(outVals);

      // Check values.
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Modify PID.
      String newChecksum =
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE + System.currentTimeMillis();
      for (HandleValue v : outVals) {
        if (v.getTypeAsString().equals(HandleMetadata.MDTYPE_CHECKSUM)) {
          v.setData(Util.encodeString(newChecksum));
        }
      }

      System.out.println("\tmodified old values to be updated");

      PidServiceUtils.printHandleValues(outVals);

      System.out.println("\tmodify values");

      PidServiceUtils.modifyHandleValues(hdlResolver, pid, outVals, authInfo);

      // Resolve again.
      System.out.println("\tresolve pid again");

      HandleValue newVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(newVals);

      // Check values again.
      assertTrue(PidServiceUtils.checkForTypeAndValue(newVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(newVals, HandleMetadata.MDTYPE_CHECKSUM,
          newChecksum));
      assertTrue(PidServiceUtils.checkForTypeAndValue(newVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, pid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test adding handle metadata values. Create a PID first.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testAddHandleMetadata() throws HandleException, IOException {

    System.out.println("testAddHandleMetadata()");

    if (testMethodsWithAuthentication) {

      System.out.println("\t" + PidServiceUtils.CREATING_PID_MESSAGE);

      // Create PID.
      HandleValue inVals[] =
          createDefaultHandleValues(TestPidServiceUtils.DEFAULT_URL_TEST_VALUE, authInfo);
      String pid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      // Resolve PID.
      System.out.println("\tresolve pid " + pid);

      HandleValue outVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(outVals);

      // Check values.
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Add metadata.
      List<HandleValue> l = new ArrayList<HandleValue>();
      for (HandleValue v : outVals) {
        l.add(v);
      }
      String testValue = "testitest";
      int testIndex = Util.getNextUnusedIndex(outVals, 1);
      HandleValue newValue = new HandleValue(testIndex, TYPE_TEST, testValue);
      HandleValue newVals[] = {newValue};

      System.out.println("\tvalue to be added");

      PidServiceUtils.printHandleValues(newVals);

      System.out.println("\tadd values");

      PidServiceUtils.addHandleValues(hdlResolver, pid, newVals, authInfo);

      // Resolve again.
      System.out.println("\tresolve pid again");

      HandleValue addedVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(addedVals);

      // Check values again.
      assertTrue(PidServiceUtils.checkForTypeAndValue(addedVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(addedVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(addedVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));
      assertTrue(PidServiceUtils.checkForTypeAndValue(addedVals, TYPE_TEST, testValue));

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, pid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Test deleting a handle. Create a PID first.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testDeleteHandle() throws HandleException, IOException {

    System.out.println("testDeleteHandle()");

    if (testMethodsWithAuthentication) {

      System.out.println("\tcreate pid with default metadata");

      // Create PID.
      HandleValue inVals[] =
          createDefaultHandleValues(TestPidServiceUtils.DEFAULT_URL_TEST_VALUE, authInfo);
      String pid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      // Resolve PID.
      System.out.println("\tresolve pid " + pid);

      HandleValue outVals[] = hdlResolver.resolveHandle(pid);
      PidServiceUtils.printHandleValues(outVals);

      // Check values.
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_URL,
          TestPidServiceUtils.DEFAULT_URL_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_CHECKSUM,
          TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE));
      assertTrue(PidServiceUtils.checkForTypeAndValue(outVals, HandleMetadata.MDTYPE_PUBDATE,
          PidServiceUtils.getCurrentDate()));

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, pid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * <p>
   * Creates a new PID. Uses server generated PID suffix (we get an UUID here). HS_ADMIN value has
   * to be created additionally for authorization reasons.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testCreateServerGeneratedHandle() throws HandleException, IOException {

    System.out.println("testCreateServerGeneratedHandle() [" + hsPrefix + "]");

    if (testMethodsWithAuthentication) {

      String url = "http://fugu.de";
      HandleValue inVals[] = createDefaultHandleValues(url, authInfo);
      String serverGeneratedPid =
          PidServiceUtils.createServerGeneratedHandle(hdlResolver, hsPrefix, inVals, authInfo);

      System.out.println("\tpid: " + serverGeneratedPid);

      assertTrue(serverGeneratedPid != null && !serverGeneratedPid.isEmpty());

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, serverGeneratedPid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * *
   * <p>
   * Creates a new PID. We use a self generated PID here (also an UUID). HS_ADMIN value has to be
   * created additionally for authorization reasons.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test
  public void testCreateSelfGeneratedHandle() throws HandleException, IOException {

    String pidSuffix = UUID.randomUUID().toString();

    System.out.println("testCreateSelfGeneratedHandle() [" + pidSuffix + "]");

    if (testMethodsWithAuthentication) {

      String pid = PidServiceUtils.getPrefixRight(hsPrefix) + pidSuffix;
      String url = "http://fugu.de";
      HandleValue inVals[] = createDefaultHandleValues(url, authInfo);
      String selfGeneratedPid =
          PidServiceUtils.createSelfGeneratedHandle(hdlResolver, pid, inVals, authInfo);

      System.out.println("\tpid: " + selfGeneratedPid);

      assertTrue(selfGeneratedPid != null && !selfGeneratedPid.isEmpty());

      // Delete PID.
      System.out.println("\tdelete pid");

      PidServiceUtils.deleteHandle(hdlResolver, selfGeneratedPid, authInfo);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
    }
  }

  /**
   * *
   * <p>
   * Try to create an existing PID.
   * </p>
   * 
   * @throws HandleException
   * @throws IOException
   */
  @Test(expected = HandleException.class)
  public void testCreateExistingHandle() throws HandleException, IOException {

    String pid =
        PidServiceUtils.getPrefixRight(hsPrefix) + "ebed3863-4425-4549-a9f1-a451e0b9252e";
    String url = "bla";

    System.out.println("testCreateExistingHandle() [" + pid + "]");

    if (testMethodsWithAuthentication) {

      try {
        HandleValue inVals[] = createDefaultHandleValues(url, authInfo);
        PidServiceUtils.createSelfGeneratedHandle(hdlResolver, pid, inVals, authInfo);
      } catch (HandleException e) {
        System.out.println("\t" + "expected exception: " + e.getMessage());
        throw e;
      }

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      // Throw expected HandleException!
      throw new HandleException(400);
    }
  }

  /**
   * <p>
   * Get a list of all handles.
   * </p>
   * 
   * @throws HandleException
   */
  @Test(expected = HandleException.class)
  public void testListHandles() throws HandleException {

    System.out.println("testListHandles() [" + hsPrefix + "]");

    if (testMethodsWithAuthentication) {

      SimpleListCallback callback = new SimpleListCallback();
      hdlResolver.listHandlesUnderPrefix(hsPrefix.toString(), authInfo, callback);

    } else {
      System.out.println("\t" + PidServiceUtils.AUTH_METHOD_TESTING_DISABLED);
      // Throw expected HandleException!
      throw new HandleException(400);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Create a default HandleValue array with URL, PUBDATE, CHECKSUM, and HS_ADMIN types.
   * </p>
   * 
   * @param theUrl
   * @param theAuthInfo
   * @return
   */
  private static HandleValue[] createDefaultHandleValues(final String theUrl,
      final AuthenticationInfo theAuthInfo) {

    // Set default metadata types.
    HandleValue url = new HandleValue(1, HandleMetadata.MDTYPE_URL, theUrl);
    HandleValue pubdate =
        new HandleValue(2, HandleMetadata.MDTYPE_PUBDATE, PidServiceUtils.getCurrentDate());
    HandleValue checksum = new HandleValue(3, HandleMetadata.MDTYPE_CHECKSUM,
        TestPidServiceUtils.DEFAULT_CHECKSUM_TEST_VALUE);
    HandleValue hsadmin = PidServiceUtils.getHsAdminValue(theAuthInfo);
    HandleValue result[] = {url, pubdate, checksum, hsadmin};

    return result;
  }

}
