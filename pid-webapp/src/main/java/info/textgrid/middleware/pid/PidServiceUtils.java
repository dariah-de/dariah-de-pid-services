/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.http.HttpResponse;
import info.textgrid.middleware.pid.api.HandleInfo;
import info.textgrid.middleware.pid.api.HandleMetadata;
import info.textgrid.middleware.pid.api.PidServiceException;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedRolesRequest;
import jakarta.ws.rs.core.Response;
import net.handle.hdllib.AbstractMessage;
import net.handle.hdllib.AbstractResponse;
import net.handle.hdllib.AddValueRequest;
import net.handle.hdllib.AdminRecord;
import net.handle.hdllib.AuthenticationInfo;
import net.handle.hdllib.CreateHandleRequest;
import net.handle.hdllib.CreateHandleResponse;
import net.handle.hdllib.DeleteHandleRequest;
import net.handle.hdllib.Encoder;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.ModifyValueRequest;
import net.handle.hdllib.ResolutionRequest;
import net.handle.hdllib.ResolutionResponse;
import net.handle.hdllib.Util;

/**
 * <p>
 * PID service utility class.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2025-02-04
 * @since 2017-07-03
 */
public class PidServiceUtils {

  public static final String SEPARATOR_CHAR = ",";
  public static final String METADATA_PATH = "/metadata";
  public static final String AUTH_METHOD_TESTING_DISABLED =
      "Auth metohd testing has been disabled!";
  public static final String NO_PROD_TESTING = "No testing on prod system!";
  public static final String CREATING_PID_MESSAGE = "Creating pid with default metadata";
  public static final String TRUE = "true";
  public static final String TYPE_HSADMIN = "HS_ADMIN";
  public static final boolean MINT_NEW_HANDLE = true;
  public static final int DEFAULT_HS_ADMIN_INDEX = 100;
  public static final int DEFAULT_ADMIN_USER_INDEX = 1;
  public static final String PID_URL_SEPARATOR_CHAR = "@";
  protected static final Logger LOG = Logger.getLogger("info.textgrid.middleware.pid");
  protected static final String ROLE_PROJECTLEADER = "Projektleiter";
  protected static final String EMPTY = "";
  protected static final String AUTH_ERROR = "Forbidden!";
  protected static final String RESOLVER_ERROR = "Error resolving Handle due to a ";
  protected static final String PID_SYNTAX_ERROR = "PID syntax not correct";
  protected static final String PID_GENERATION_ERROR = "Generated PID is null or blank";
  protected static final String SUFFIX_ERROR = "PID suffix is null or blank";
  protected static final String PID_ARGUMENT_MISMATCH =
      "PID argument must be not empty and equal for suffix and URI parameter lists";
  private static final String ERROR_URL_MISSING = "";
  private static final String ERROR_FILESIZE_MISSING = "";
  private static final String ERROR_CHECKSUM_MISSING = "";
  private static final String ERROR_publisher_MISSING = "";
  private static final String ERROR_CREATOR_MISSING = "";

  /**
   * @return The current date in format like "1970-12-10".
   */
  public static String createPubDate() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    return dateFormat.format(date);
  }

  /**
   * <p>
   * Defines and starts the logging infrastructure.
   * </p>
   * 
   * @param theLogLocation
   * @param theLoglevel
   * @throws IOException
   */
  protected static void startLogging(final String theLogLocation,
      final String theLoglevel)
      throws IOException {

    // At the very beginning turn off the parent loggers console output.
    LOG.setUseParentHandlers(false);

    // Set the file log handlers and assign them to our logger.
    FileHandler fh = new FileHandler(theLogLocation, true);
    if (theLoglevel != null) {
      fh.setLevel(Level.parse(theLoglevel));
    } else {
      fh.setLevel(Level.INFO);
    }
    Formatter sf = new SimpleFormatter();
    fh.setFormatter(sf);
    LOG.addHandler(fh);
    LOG.log(Level.INFO,
        "PID service logging up and running [" + fh.getLevel() + "]. Writing to " + theLogLocation);
  }

  /**
   * @param theHdlValues The handle value array to print out.
   */
  public static void printHandleValues(final HandleValue theHdlValues[]) {
    for (HandleValue h : theHdlValues) {
      System.out
          .println("\t" + h.getIndex() + "\t" + h.getTypeAsString() + "\t" + h.getDataAsString());
    }
  }

  /**
   * @param theHdlValues The handle value array to log.
   */
  public static void logHandleValues(final HandleValue theHdlValues[]) {
    for (HandleValue h : theHdlValues) {
      LOG.fine(h.getIndex() + "\t" + h.getTypeAsString() + "\t" + h.getDataAsString());
    }
  }

  /**
   * <p>
   * Check for a certain value of a certain TYPE in the given Handle Metadata.
   * </p>
   * 
   * @param theHdlValues The handle value array.
   * @param theType The metadata type to check for a value.
   * @param theValue The value to check on given metadata type.
   * @return true if type and value is existing, false if not.
   */
  public static boolean checkForTypeAndValue(final HandleValue theHdlValues[],
      final String theType,
      final String theValue) {

    boolean result = false;

    for (HandleValue h : theHdlValues) {
      if (h.getTypeAsString().equals(theType)) {
        if (h.getDataAsString().equals(theValue)) {
          result = true;
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Checks if a certain TYPE is existing in the given Handle metadata.
   * </p>
   * 
   * @param theHdlValues The handle value array.
   * @param theType The metadata type to check.
   * @return
   */
  public static boolean checkForType(final HandleValue theHdlValues[],
      final String theType) {

    boolean result = false;

    for (HandleValue h : theHdlValues) {
      if (h.getTypeAsString().equals(theType)) {
        result = true;
        break;
      }
    }

    return result;
  }

  /**
   * <p>
   * Checks for the TG-pid Service password.
   * </p>
   * 
   * @param thePidServiceSecret
   * @param theAuth
   * @return true if authenticated, false otherwise.
   */
  public static boolean checkAccess(final String thePidServiceSecret,
      final String theAuth) {

    boolean result = false;

    if (thePidServiceSecret.equals(theAuth)) {
      result = true;
    }

    LOG.info("CHECKACCESS via pid secret: " + result);

    return result;
  }

  /**
   * <p>
   * Check if the given session ID is allowed to use PID Service.
   * </p>
   * 
   * @param theTgAuthService
   * @param theSessionID
   * @return true if authenticated, false otherwise.
   */
  public static boolean checkAccess(final PortTgextra theTgAuthService,
      final String theSessionID) {

    boolean result = false;

    TgAssignedRolesRequest rolesRequest = new TgAssignedRolesRequest();
    rolesRequest.setAuth(theSessionID);
    RolesetResponse response = theTgAuthService.tgAssignedRoles(rolesRequest);

    // Check if the user assigned to the session ID has the role "Project leader" (in any project).
    if (response.getRole() != null) {
      for (String role : response.getRole()) {

        LOG.info("  ##  role: " + role);

        if (role.startsWith(ROLE_PROJECTLEADER)) {
          result = true;
          break;
        }
      }
    }

    LOG.info("CHECKACCESS via rbac sid: " + result);

    return result;
  }

  /**
   * @param theResolver The Handle Resolver object.
   * @param thePid The PID to get the type value for.
   * @param theType The metadata type to get the value for.
   * @return Metadata value string, if the metadata type is existing, empty string otherwise.
   * @throws IOException
   */
  public static String getHandleTypeValue(final HandleResolver theResolver,
      final String thePid,
      final String theType)
      throws IOException {

    String result = "";

    try {
      String type2look4[] = {theType};
      HandleValue vals[] = theResolver.resolveHandle(thePid, type2look4, null);

      result = getHandleTypeValue(vals, theType);

      LOG.info("type=" + theType + ": " + result);

    } catch (HandleException e) {
      throw new IOException(RESOLVER_ERROR + e.getClass().getName() + ": " + e.getMessage());
    }

    return result;
  }

  /**
   * @param theResolver The Handle Resolver object.
   * @param thePid The PID to get the type value for.
   * @param theRequestedTypes The requested metadata types to get values for.
   * @return The array of handle value objects for all the types.
   * @throws HandleException
   */
  public static HandleValue[] resolveAuthoritative(final HandleResolver resolver,
      final String thePid,
      final String theRequestedTypes[])
      throws HandleException {

    HandleValue result[];

    ResolutionRequest req = new ResolutionRequest(
        Util.encodeString(thePid),
        convert2Bytes(theRequestedTypes),
        null,
        null);

    req.authoritative = true;
    AbstractResponse response = resolver.processRequest(req);

    // Check the response to see if the operation was successful.
    if (response.responseCode == AbstractMessage.RC_SUCCESS) {

      // The resolution was successful, so we'll cast the response and set return value.
      result = ((ResolutionResponse) response).getHandleValues();

      return result;

    } else {
      String message = "error resolving " + thePid;
      throw new HandleException(response.responseCode, message);
    }
  }

  /**
   * <p>
   * Get the value for a certain TYPE from the given Handle Metadata.
   * </p>
   * 
   * @param theHdlValues
   * @param theType
   * @return
   */
  public static String getHandleTypeValue(final HandleValue theHdlValues[],
      String theType) {

    String result = "";

    for (HandleValue h : theHdlValues) {
      if (h.getTypeAsString().equals(theType)) {
        result = h.getDataAsString();
      }
    }

    return result;
  }

  /**
   * @param theAuth
   * @param theCreator
   * @return
   */
  public static HandleValue[] getDefaultDhrepValues(final AuthenticationInfo theAuth,
      final String theCreator) {

    // DH-rep creates Handles first using DH-crud#GETURI, and adds more metadata later in theDH-crud
    // creation process! So we use index 1 for CREATOR by default.
    HandleValue creatorValue = new HandleValue(1, HandleMetadata.MDTYPE_CREATOR, theCreator);
    HandleValue result[] = {creatorValue, getHsAdminValue(theAuth)};

    logHandleValues(result);

    return result;
  }

  /**
   * @param theAuth
   * @param theUrl
   * @param theFilesize
   * @param theChecksum
   * @param thePublisher
   * @param theCreator
   * @return
   * @throws PidServiceException
   */
  public static HandleValue[] getDefaultTgrepValues(final AuthenticationInfo theAuth,
      final String theVersion,
      HandleInfo theInfo)
      throws PidServiceException {

    if (theInfo.metadata.url == null) {
      throw new PidServiceException(ERROR_URL_MISSING);
    }
    if (theInfo.metadata.filesize == null) {
      throw new PidServiceException(ERROR_FILESIZE_MISSING);
    }
    if (theInfo.metadata.checksum == null) {
      throw new PidServiceException(ERROR_CHECKSUM_MISSING);
    }
    if (theInfo.metadata.tgPublisher == null) {
      throw new PidServiceException(ERROR_publisher_MISSING);
    }
    if (theVersion == null) {
      throw new PidServiceException(ERROR_CREATOR_MISSING);
    }

    // 1 -- URL 2021-09-17 -- https://textgridrep.org/textgrid:3tmfz.0
    HandleValue url = new HandleValue(1, HandleMetadata.MDTYPE_URL, theInfo.metadata.url);
    // 2 -- METADATA_URL 2021-09-17 -- https://textgridrep.org/textgrid:3tmfz.0/metadata
    HandleValue metadataUrl =
        new HandleValue(2, HandleMetadata.MDTYPE_TG_METADATA, theInfo.metadata.url + METADATA_PATH);
    // 3 -- FILESIZE 2024-09-17 -- 525
    HandleValue filesize =
        new HandleValue(3, HandleMetadata.MDTYPE_FILESIZE, theInfo.metadata.filesize);
    // 4 -- CHECKSUM 2024-09-17 -- md5:ed9e6d880b3f5a775a3205e218ceb2f6
    HandleValue checksum =
        new HandleValue(4, HandleMetadata.MDTYPE_CHECKSUM, theInfo.metadata.checksum);
    // 5 -- PUBLISHER 2021-09-17 -- TextGrid
    HandleValue publisher =
        new HandleValue(5, HandleMetadata.MDTYPE_TG_PUBLISHER, theInfo.metadata.tgPublisher);
    // 6 -- PUBDATE 2021-09-17 -- 2021-09-17
    HandleValue pubdate = new HandleValue(6, HandleMetadata.MDTYPE_PUBDATE, getCurrentDate());
    // 7 -- CREATOR 2021-09-17 -- PID Service pid-webapp-5.3.1-SNAPSHOT+202109131734
    HandleValue creator = new HandleValue(7, HandleMetadata.MDTYPE_CREATOR, theVersion);
    // 100 -- HS_ADMIN
    HandleValue hsadmin = getHsAdminValue(theAuth);

    HandleValue result[] =
        {url, metadataUrl, filesize, checksum, publisher, pubdate, creator, hsadmin};

    logHandleValues(result);

    return result;
  }

  /**
   * <p>
   * Creates a default HS_ADMIN metadata value.
   * </p>
   * 
   * @param theAuth The auth object.
   * @return The HS_ADMIN HandleValue.
   */
  public static HandleValue getHsAdminValue(final AuthenticationInfo theAuth) {

    HandleValue result = new HandleValue();

    AdminRecord hsadmin = new AdminRecord(
        theAuth.getUserIdHandle(),
        DEFAULT_ADMIN_USER_INDEX,
        AdminRecord.PRM_ADD_HANDLE,
        AdminRecord.PRM_DELETE_HANDLE,
        AdminRecord.PRM_NO_ADD_NA,
        AdminRecord.PRM_NO_DELETE_NA,
        AdminRecord.PRM_MODIFY_VALUE,
        AdminRecord.PRM_REMOVE_VALUE,
        AdminRecord.PRM_ADD_VALUE,
        AdminRecord.PRM_MODIFY_ADMIN,
        AdminRecord.PRM_REMOVE_ADMIN,
        AdminRecord.PRM_ADD_ADMIN,
        AdminRecord.PRM_READ_VALUE,
        AdminRecord.PRM_NO_LIST_HANDLES);

    result.setData(Encoder.encodeAdminRecord(hsadmin));
    result.setType(Util.encodeString(TYPE_HSADMIN));
    result.setIndex(DEFAULT_HS_ADMIN_INDEX);
    result.setAdminCanRead(true);
    result.setAdminCanWrite(true);
    result.setAnyoneCanRead(true);
    result.setAnyoneCanWrite(false);

    return result;
  }

  /**
   * <p>
   * Create a newly minted server generated Handle PID.
   * </p>
   * 
   * @param theResolver
   * @param thePrefix
   * @param theValues
   * @param theAuthInfo
   * @return
   * @throws HandleException
   */
  public static String createServerGeneratedHandle(final HandleResolver theResolver,
      final String thePrefix, final HandleValue theValues[], final AuthenticationInfo theAuthInfo)
      throws HandleException {

    String result = "";

    String prefix = getPrefixRight(thePrefix);

    LOG.info("Requesting new pid for prefix: " + thePrefix);

    // Create and send the create request.
    CreateHandleRequest request = new CreateHandleRequest(
        Util.encodeString(prefix),
        theValues,
        theAuthInfo,
        PidServiceUtils.MINT_NEW_HANDLE);
    AbstractResponse response = theResolver.processRequest(request);

    // Check the response to see if the operation was successful.
    if (response.responseCode == AbstractMessage.RC_SUCCESS) {
      // The creation was successful, so we'll cast the response and get the handle values.
      result = Util.decodeString(((CreateHandleResponse) response).handle);
    } else {
      String message = response.responseCode + " "
          + AbstractMessage.getResponseCodeMessage(response.responseCode);
      throw new HandleException(response.responseCode, message);
    }

    return result;
  }

  /**
   * <p>
   * Create a self generated Handle PID.
   * </p>
   * 
   * @param theResolver
   * @param thePid
   * @param theValues
   * @param theAuthInfo
   * @return
   * @throws HandleException
   */
  public static String createSelfGeneratedHandle(final HandleResolver theResolver,
      final String thePid, final HandleValue theValues[], final AuthenticationInfo theAuthInfo)
      throws HandleException {

    String result = "";

    LOG.info("Requesting pid: " + thePid);

    // Create and send the create request.
    CreateHandleRequest request = new CreateHandleRequest(
        Util.encodeString(thePid),
        theValues,
        theAuthInfo);
    AbstractResponse response = theResolver.processRequest(request);

    // Check the response to see if the operation was successful.
    if (response.responseCode == AbstractMessage.RC_SUCCESS) {
      // The creation was successful, so we'll cast the response and get the handle values.
      result = Util.decodeString(((CreateHandleResponse) response).handle);
    } else {
      String message = response.responseCode + " "
          + AbstractMessage.getResponseCodeMessage(response.responseCode);
      throw new HandleException(response.responseCode, message);
    }

    return result;
  }

  /**
   * <p>
   * Modify the handle values of an existing PID, only existing values are being updated.
   * </p>
   * 
   * @param theResolver
   * @param thePid
   * @param theHandleValues
   * @param theAuthInfo
   * @throws HandleException
   */
  public static void modifyHandleValues(final HandleResolver theResolver,
      final String thePid,
      final HandleValue theHandleValues[],
      final AuthenticationInfo theAuthInfo)
      throws HandleException {

    LOG.info("Modifying pid: " + thePid);

    // Create and send the update request.
    ModifyValueRequest request = new ModifyValueRequest(
        Util.encodeString(thePid),
        theHandleValues,
        theAuthInfo);
    AbstractResponse response = theResolver.processRequest(request);

    // Check the response to see if the operation was successful.
    if (response.responseCode != AbstractMessage.RC_SUCCESS) {
      String message = response.responseCode + " "
          + AbstractMessage.getResponseCodeMessage(response.responseCode);
      throw new HandleException(response.responseCode, message);
    }
  }

  /**
   * <p>
   * Add handle values to an existing PID, only not yet existing values are being created.
   * </p>
   * 
   * @param theResolver
   * @param thePid
   * @param theHandleValues
   * @param theAuthInfo
   * @throws HandleException
   */
  public static void addHandleValues(final HandleResolver theResolver,
      final String thePid,
      final HandleValue theHandleValues[],
      final AuthenticationInfo theAuthInfo)
      throws HandleException {

    LOG.info("Adding values to pid: " + thePid);

    // Create and send the update request.
    AddValueRequest request = new AddValueRequest(
        Util.encodeString(thePid),
        theHandleValues,
        theAuthInfo);
    AbstractResponse response = theResolver.processRequest(request);

    // Check the response to see if the operation was successful.
    if (response.responseCode != AbstractMessage.RC_SUCCESS) {
      String message = response.responseCode + " "
          + AbstractMessage.getResponseCodeMessage(response.responseCode);
      throw new HandleException(response.responseCode, message);
    }
  }

  /**
   * <p>
   * Delete a PID.
   * </p>
   * 
   * @param theResolver
   * @param thePid
   * @param theAuthInfo
   * @throws HandleException
   */
  public static void deleteHandle(final HandleResolver theResolver,
      final String thePid,
      final AuthenticationInfo theAuthInfo)
      throws HandleException {

    LOG.info("Deleting pid: " + thePid);

    // Delete handle.
    DeleteHandleRequest request = new DeleteHandleRequest(
        Util.encodeString(thePid),
        theAuthInfo);
    AbstractResponse response = theResolver.processRequest(request);

    // Check the response to see if the operation was successful.
    if (response.responseCode != AbstractMessage.RC_SUCCESS) {
      String message = response.responseCode + " "
          + AbstractMessage.getResponseCodeMessage(response.responseCode);
      throw new HandleException(response.responseCode, message);
    }
  }

  /**
   * <p>
   * Return the current date in 2025-01-10 format.
   * </p>
   * 
   * @return
   */
  public static String getCurrentDate() {
    return (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
  }

  /**
   * <p>
   * Get the prefix right for creation!
   * </p>
   * 
   * <p>
   * THE PREFIX for the CreateHandleRequest MUST END WITH A "/" !!!!
   * </p>
   * 
   * @param thePrefix
   * @return The Handle prefix with a "/" at the end!
   */
  public static String getPrefixRight(final String thePrefix) {
    return thePrefix + (thePrefix.endsWith("/") ? "" : "/");
  }

  /**
   * <p>
   * Convert the key/value pairs from the HandleInfo object to a HandleValue array. Existing
   * metadata is used to determine the index of the new metadata.
   * </p>
   * 
   * @param theExistingValues The handle values from a resolved handle.
   * @param theValuesToAdd The key value list for adding metadata.
   * @return The HandleValue array to add to the Handle metadata.
   * @throws PidServiceException
   */
  public static HandleValue[] getAddedHandleValues(final HandleValue theExistingValues[],
      final HandleInfo theValuesToAdd)
      throws PidServiceException {

    // Create result list.
    List<HandleValue> resultList = new ArrayList<HandleValue>();

    // Get next index for new metadata.
    int nextIndex = Util.getNextUnusedIndex(theExistingValues, 1);

    // Get temp map with existing metadata types.
    HashSet<String> existingTypes = new HashSet<String>();
    for (HandleValue v : theExistingValues) {
      existingTypes.add(v.getTypeAsString());
    }

    // Iterate the metadata map of HandleInfo to add.
    for (String typeToAdd : theValuesToAdd.metadata.getMetadataMap().keySet()) {
      // Create HandleValue object, if not yet existing.
      if (!existingTypes.contains(typeToAdd)) {
        HandleValue newValue = new HandleValue(nextIndex, typeToAdd,
            theValuesToAdd.metadata.getMetadataMap().get(typeToAdd));
        resultList.add(newValue);
        // Increase counters.
        nextIndex++;
      }
    }

    return resultList.toArray(new HandleValue[resultList.size()]);
  }

  /**
   * <p>
   * Convert the key/value pairs from the Handle Info object to a HandleValue array using the old
   * values from resolved handle.
   * </p>
   * 
   * @param theExistingValues The handle values from a resolved handle.
   * @param theValuesToUpdate The HandleInfo object for updating the old handle values.
   * @return The HandleValue array for updating/modifying the Handle metadata.
   * @throws PidServiceException
   */
  public static HandleValue[] getUpdatedHandleValues(final HandleValue theExistingValues[],
      final HandleInfo theValuesToUpdate)
      throws PidServiceException {

    HandleValue[] result = theExistingValues;

    // Iterate over the existing metadata types and update to new values, if type existing in
    // HandleInfo.
    for (HandleValue v : result) {
      String typeToUpdate = v.getTypeAsString();
      if (theValuesToUpdate.metadata.getMetadataMap().containsKey(typeToUpdate)) {
        v.setData(Util.encodeString(theValuesToUpdate.metadata.getMetadataMap().get(typeToUpdate)));
      }
    }

    return result;
  }

  /**
   * @param e
   * @return
   */
  public static PidServiceException newTGPidError(final Exception e) {

    String errorMessage = "ERROR! " + e.getClass().getName() + ": " + e.getMessage();

    return newTGPidError(errorMessage);
  }

  /**
   * @param m
   * @return
   */
  public static PidServiceException newTGPidError(final String m) {

    LOG.severe(m);

    return new PidServiceException(m);
  }

  /**
   * @param httpres
   * @param text
   * @return
   * @throws IOException
   */
  public static Response handleHTTPRequestError(final HttpResponse httpres,
      final String text)
      throws IOException {

    BufferedReader br =
        new BufferedReader(new InputStreamReader((httpres.getEntity().getContent())));
    String output = null;
    PidServiceUtils.LOG.fine("Output from contacted Server .... \n");
    while ((output = br.readLine()) != null) {
      PidServiceUtils.LOG.fine(output);
    }

    PidServiceUtils.LOG.fine(output);

    Response r = Response.status(httpres.getStatusLine().getStatusCode())
        .entity(text + " " + httpres.getStatusLine() + " " + output).build();

    return r;
  }

  /**
   * <p>
   * Do convert from String array to two dimensional byte array, see
   * HandleResolver.resolveHandle(String sHandle, String sTypes[], int indexes[]).
   * </p>
   * 
   * @param sTypes
   * @return the byte array or null if not
   * @throws HandleException
   */
  public static byte[][] convert2Bytes(final String sTypes[])
      throws HandleException {

    String income[] = sTypes;
    if (sTypes == null) {
      income = new String[0];
    }

    byte result[][] = new byte[income.length][];

    // Convert the types and handle to UTF8 byte-strings.
    for (int i = 0; i < income.length; i++) {
      result[i] = Util.encodeString(income[i]);
    }

    return result;
  }

  /**
   * <p>
   * Get value array out of given CSV string.
   * </p>
   * 
   * @param theCsvString The CSV string to convert.
   * @return The array of trimmed values from the given CSV string.
   */
  public static String[] convertCsv2Array(final String theCsvString) {

    String result[];

    String toWorkWith = theCsvString;
    if (theCsvString == null) {
      toWorkWith = "";
    }

    // Trim and remove trailing separator chars.
    String pidSuffixesString = toWorkWith.trim();
    if (pidSuffixesString.endsWith(PidServiceUtils.SEPARATOR_CHAR)) {
      pidSuffixesString = pidSuffixesString.substring(0,
          pidSuffixesString.lastIndexOf(PidServiceUtils.SEPARATOR_CHAR));
    }

    // Split string and get array.
    result = pidSuffixesString.split(PidServiceUtils.SEPARATOR_CHAR);

    // Trim single values.
    for (int i = 0; i < result.length; i++) {
      result[i] = result[i].trim();
    }

    return result;
  }

  /**
   * @param theHdlResolver The HDLLIB resolver.
   * @param theHsPrefix The HS Prefix for HDLLIB.
   * @param theVersion The PID Service version.
   * @param theHandleInfo The HandleInfo object containing suffix, and all needed Handle metadata.
   * @param theAuthInfo The auth for HDLLIB.
   * @return A HandleInfo object with suffix, handle, and URL only.
   * @throws PidServiceException
   */
  public static HandleInfo getSingleTextgridPid(HandleResolver theHdlResolver, String theHsPrefix,
      String theVersion, HandleInfo theHandleInfo, AuthenticationInfo theAuthInfo)
      throws PidServiceException {

    HandleInfo result;

    // Check for suffix.
    if (theHandleInfo.suffix == null || theHandleInfo.suffix.isBlank()) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.SUFFIX_ERROR);
    }

    result = new HandleInfo(theHandleInfo.suffix, theHandleInfo.metadata.url);

    // Create default Handle metadata values for TextGrid PID.
    HandleValue values[] = PidServiceUtils.getDefaultTgrepValues(
        theAuthInfo,
        theVersion,
        theHandleInfo);

    // Assemble PID.
    String pidString = theHsPrefix + theHandleInfo.suffix;

    // Create PID.
    try {
      String generatedPid = PidServiceUtils.createSelfGeneratedHandle(
          theHdlResolver,
          pidString,
          values,
          theAuthInfo);

      if (generatedPid == null || generatedPid.isBlank()) {
        throw PidServiceUtils.newTGPidError(PidServiceUtils.PID_GENERATION_ERROR);
      }

      result.handle = generatedPid;
      result.metadata.url = theHandleInfo.metadata.url;

      PidServiceUtils.LOG.fine("PID created: " + result.handle);

    } catch (HandleException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    return result;
  }

}
