package info.textgrid.middleware.pid;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.xml.bind.DatatypeConverter;

/**
 * <p>
 * Thanks for the tip: https://www.adam-bien.com/roller/abien/entry/client_side_http_basic_access
 * </p>
 */
public class PidServiceAuthenticator implements ClientRequestFilter {

  // **
  // FINALS
  // **

  private static final String AUTH = "Authorization";
  private static final String UTF = "UTF-8";

  // **
  // CLASS
  // **

  private final String user;
  private final String pass;

  /**
   * @param user
   * @param pass
   */
  public PidServiceAuthenticator(String theUser, String thePass) {
    this.user = theUser;
    this.pass = thePass;
  }

  /**
   *
   */
  public void filter(ClientRequestContext theRequestContext) throws IOException {
    MultivaluedMap<String, Object> headers = theRequestContext.getHeaders();
    final String basicAuthentication = getBasicAuthentication();
    headers.add(AUTH, basicAuthentication);
  }

  /**
   * @return
   */
  private String getBasicAuthentication() {

    String tok = this.user + ":" + this.pass;

    try {
      return "BASIC " + DatatypeConverter.printBase64Binary(tok.getBytes(UTF));
    } catch (UnsupportedEncodingException e) {
      throw new IllegalStateException("No encoding possible for " + UTF, e);
    }
  }

}
