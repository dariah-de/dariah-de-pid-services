/**
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * SUB Göttingen (http://www.sub.uni-goettingen.de)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 * 
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * 
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Johannes Biermann (biermann@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.pid.doi;

import java.util.ArrayList;
import java.util.List;

/*******************************************************************************
 * <p>
 * Bean for storing the data from the RDF metadata object
 * </p>
 * 
 * @author Johannes Biermann, SUB Göttingen
 * @version 2018-03-08
 * @since 2017-06-22
 ******************************************************************************/

public class MetadataBean {

  private String hdl;
  private List<String> contributor = new ArrayList<String>();
  private List<String> coverage = new ArrayList<String>();
  private List<String> creator = new ArrayList<String>();
  private List<String> date = new ArrayList<String>();
  private List<String> description = new ArrayList<String>();
  private List<String> format = new ArrayList<String>();
  private List<String> identifier = new ArrayList<String>();
  private List<String> language = new ArrayList<String>();
  private List<String> publisher = new ArrayList<String>();
  private List<String> relation = new ArrayList<String>();
  private List<String> rights = new ArrayList<String>();
  private List<String> source = new ArrayList<String>();
  private List<String> subject = new ArrayList<String>();
  private List<String> title = new ArrayList<String>();
  private List<String> type = new ArrayList<String>();

  /* normal getters and setters */

  /**
   * @return
   */
  public String getHdl() {
    return this.hdl;
  }

  /**
   * @return
   */
  public List<String> getContributor() {
    return this.contributor;
  }

  /**
   * @return
   */
  public List<String> getCoverage() {
    return this.coverage;
  }

  /**
   * @return
   */
  public List<String> getCreator() {
    return this.creator;
  }

  /**
   * @return
   */
  public List<String> getDate() {
    return this.date;
  }

  /**
   * @return
   */
  public List<String> getDescription() {
    return this.description;
  }

  /**
   * @return
   */
  public List<String> getFormat() {
    return this.format;
  }

  /**
   * @return
   */
  public List<String> getIdentifier() {
    return this.identifier;
  }

  /**
   * @return
   */
  public List<String> getLanguage() {
    return this.language;
  }

  /**
   * @return
   */
  public List<String> getPublisher() {
    return this.publisher;
  }

  /**
   * @return
   */
  public List<String> getRelation() {
    return this.relation;
  }

  /**
   * @return
   */
  public List<String> getRights() {
    return this.rights;
  }

  /**
   * @return
   */
  public List<String> getSource() {
    return this.source;
  }

  /**
   * @return
   */
  public List<String> getSubject() {
    return this.subject;
  }

  /**
   * @return
   */
  public List<String> getTitle() {
    return this.title;
  }

  /**
   * @return
   */
  public List<String> getType() {
    return this.type;
  }

  /**
   * @param hdl
   */
  public void setHdl(String hdl) {
    this.hdl = hdl;
  }

  /**
   * @param contributor
   */
  public void setContributor(List<String> contributor) {
    this.contributor = contributor;
  }

  /**
   * @param coverage
   */
  public void setCoverage(List<String> coverage) {
    this.coverage = coverage;
  }

  /**
   * @param creator
   */
  public void setCreator(List<String> creator) {
    this.creator = creator;
  }

  /**
   * @param date
   */
  public void setDate(List<String> date) {
    this.date = date;
  }

  /**
   * @param description
   */
  public void setDescription(List<String> description) {
    this.description = description;
  }

  /**
   * @param format
   */
  public void setFormat(List<String> format) {
    this.format = format;
  }

  /**
   * @param identifier
   */
  public void setIdentifier(List<String> identifier) {
    this.identifier = identifier;
  }

  /**
   * @param language
   */
  public void setLanguage(List<String> language) {
    this.language = language;
  }

  /**
   * @param publisher
   */
  public void setPublisher(List<String> publisher) {
    this.publisher = publisher;
  }

  /**
   * @param relation
   */
  public void setRelation(List<String> relation) {
    this.relation = relation;
  }

  /**
   * @param rights
   */
  public void setRights(List<String> rights) {
    this.rights = rights;
  }

  /**
   * @param source
   */
  public void setSource(List<String> source) {
    this.source = source;
  }

  /**
   * @param subject
   */
  public void setSubject(List<String> subject) {
    this.subject = subject;
  }

  /**
   * @param title
   */
  public void setTitle(List<String> title) {
    this.title = title;
  }

  /**
   * @param type
   */
  public void setType(List<String> type) {
    this.type = type;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "MetadataBean [hdl=" + this.hdl + ", contributor=" + this.contributor + ", coverage="
        + this.coverage + ", creator=" + this.creator + ", date=" + this.date + ", description="
        + this.description + ", format=" + this.format + ", identifier=" + this.identifier
        + ", language=" + this.language + ", publisher=" + this.publisher + ", relation="
        + this.relation + ", rights=" + this.rights + ", source=" + this.source + ", subject="
        + this.subject + ", title=" + this.title + ", type=" + this.type + "]";
  }

}
