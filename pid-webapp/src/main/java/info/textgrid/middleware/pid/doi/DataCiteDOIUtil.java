/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Wolfgang Pempe (pempe@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 * @author Johannes Biermann (biermann@sub-uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.pid.doi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.jena.rdf.model.LiteralRequiredException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.xml.sax.SAXException;
import info.textgrid.middleware.pid.doi.generated.ContributorType;
import info.textgrid.middleware.pid.doi.generated.DateType;
import info.textgrid.middleware.pid.doi.generated.DescriptionType;
import info.textgrid.middleware.pid.doi.generated.ObjectFactory;
import info.textgrid.middleware.pid.doi.generated.RelatedIdentifierType;
import info.textgrid.middleware.pid.doi.generated.RelationType;
import info.textgrid.middleware.pid.doi.generated.Resource.Contributors;
import info.textgrid.middleware.pid.doi.generated.Resource.Contributors.Contributor;
import info.textgrid.middleware.pid.doi.generated.Resource.Creators;
import info.textgrid.middleware.pid.doi.generated.Resource.Creators.Creator;
import info.textgrid.middleware.pid.doi.generated.Resource.Dates;
import info.textgrid.middleware.pid.doi.generated.Resource.Descriptions;
import info.textgrid.middleware.pid.doi.generated.Resource.Descriptions.Description;
import info.textgrid.middleware.pid.doi.generated.Resource.Formats;
import info.textgrid.middleware.pid.doi.generated.Resource.GeoLocations;
import info.textgrid.middleware.pid.doi.generated.Resource.GeoLocations.GeoLocation;
import info.textgrid.middleware.pid.doi.generated.Resource.Identifier;
import info.textgrid.middleware.pid.doi.generated.Resource.RelatedIdentifiers;
import info.textgrid.middleware.pid.doi.generated.Resource.RelatedIdentifiers.RelatedIdentifier;
import info.textgrid.middleware.pid.doi.generated.Resource.RightsList;
import info.textgrid.middleware.pid.doi.generated.Resource.RightsList.Rights;
import info.textgrid.middleware.pid.doi.generated.Resource.Subjects;
import info.textgrid.middleware.pid.doi.generated.Resource.Subjects.Subject;
import info.textgrid.middleware.pid.doi.generated.Resource.Titles;
import info.textgrid.middleware.pid.doi.generated.Resource.Titles.Title;
import info.textgrid.middleware.pid.doi.generated.ResourceType;

/*******************************************************************************
 * <p>
 * This Class reads the DARIAH RDF object into the MetadataBean and converts it into the DataCite
 * metadata scheme.
 * </p>
 * 
 * @author Johannes Biermann, SUB Göttingen
 * @version 2018-03-08
 * @since 2017-06-22
 ******************************************************************************/

public class DataCiteDOIUtil {

  protected static final Logger LOG = Logger.getLogger("info.textgrid.middleware.pid");

  private static final String DARIAH_NS = "http://de.dariah.eu/rdf/dataobjects/terms/";
  private static final String DC_NS = "http://purl.org/dc/elements/1.1/";
  private static final String DATACITE_SCHEMA_LOCATION =
      "http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd";

  private static final String DARIAH_COLLECTION = "text/vnd.dariah.dhrep.collection+turtle";

  /**
   * This method reads a repository RDF object and returns it as Java Bean. the repository object
   * can be a Collection or DataObject. Note: It only returns the first dataset from the RDF object
   * if the RDF object contains multiple entries!
   * 
   * @param hdlIdentifier
   * @param rdfObject
   * @param doiPrefix
   * @return
   */
  public static MetadataBean readMetadataFromRDF(String hdlIdentifier, String rdfObject,
      String doiPrefix) {

    Model model = ModelFactory.createDefaultModel();
    model.read(new ByteArrayInputStream(rdfObject.getBytes()), null, "TURTLE");
    MetadataBean metadata = null;

    // we have defined that we get the metadata element for 1 object at a
    // time.
    // therefore we look if we have a collection object in the data or a
    // data object. If both are present, we prefer the collection this can
    // be changed by fetching data for both or use the inference mechanism
    // from jena to resolve the hasPart links from the collection object
    Resource resItem = model.getResource(DARIAH_NS + "Collection");
    // does the model contain a collection or a data object?
    if (false == model.containsResource(resItem)) {
      resItem = model.getResource(DARIAH_NS + "DataObject");
    }

    ResIterator resDataset = model.listSubjectsWithProperty(RDF.type, resItem);
    // attention: to retrieve more than one item replace this if clause with
    // while (resDataset.hasNext()) {
    // if needed
    if (resDataset.hasNext()) {

      metadata = new MetadataBean();

      metadata.setHdl(hdlIdentifier);

      Resource dataset = resDataset.next();

      /*
       * yes it is a bit ugly, but we have to repeat this manually for every element type it is a
       * shortcut to Property property =
       * model.getProperty("http://purl.org/dc/elements/1.1/format"); StmtIterator it =
       * dataset.listProperties( property ); we fill the bean with the corresponding metadata field
       */
      metadata.setContributor(retrieveMetadataElement(
          dataset.listProperties(model.getProperty(DC_NS + "contributor"))));

      metadata.setCoverage(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "coverage"))));

      metadata.setCreator(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "creator"))));

      metadata.setDate(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "date"))));

      metadata.setDescription(retrieveMetadataElement(
          dataset.listProperties(model.getProperty(DC_NS + "description"))));

      metadata.setFormat(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "format"))));

      metadata.setIdentifier(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "identifier"))));

      metadata.setLanguage(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "language"))));

      metadata.setPublisher(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "publisher"))));

      metadata.setRelation(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "relation"))));

      metadata.setRights(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "rights"))));

      metadata.setSource(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "source"))));

      metadata.setSubject(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "subject"))));

      metadata.setTitle(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "title"))));

      metadata.setType(
          retrieveMetadataElement(dataset.listProperties(model.getProperty(DC_NS + "type"))));
    }

    return metadata;
  }

  /**
   * This method converts the metadata bean (which represents the DC metadata) to a datacite XML
   * metadata document. It maps the fields as best as it can. Most of the metadata elements are
   * compatible, some elements from datacite allow only single values, multiple values are ignored.
   * Note that the identifier element is ignored since we need to specify the type which we don't
   * know. as primary identifier we set the DOI prefix + handle ID. We set the Handle ID in
   * relatedIdentifier.
   * 
   * @param rdfObject
   * @param hdlIdentifier
   * @param validate
   * @param doiPrefix
   * @param doiPublisher
   * @return generated XML as String
   * @throws JAXBException
   * @throws UnsupportedEncodingException
   * @throws SAXException
   */
  public static String convertMetadataToDataciteXML(String rdfObject, String hdlIdentifier,
      boolean validate, String doiPrefix, String doiPublisher)
      throws UnsupportedEncodingException, JAXBException, SAXException {

    // convert the RDF object to a Java POJO
    MetadataBean metadata = readMetadataFromRDF(hdlIdentifier, rdfObject, doiPrefix);

    String result = null;
    if (null == metadata) {
      return null;
    }

    // initialize the JAXB factory
    ObjectFactory factory = new ObjectFactory();
    info.textgrid.middleware.pid.doi.generated.Resource resource = factory.createResource();

    // first we set the DOI identifier, basically we use the Handle ID plus DOI prefix
    Identifier ri = factory.createResourceIdentifier();
    ri.setValue(getDOIIdentifier(metadata.getHdl(), doiPrefix));
    ri.setIdentifierType("DOI");
    resource.setIdentifier(ri);

    // now we need to set the mandatory attributes for the datacite metadata schema

    // one or more titles
    //
    Titles titles = factory.createResourceTitles();
    for (String title : metadata.getTitle()) {
      Title t = factory.createResourceTitlesTitle();
      t.setValue(title);
      titles.getTitle().add(t);
    }
    resource.setTitles(titles);

    // publisher
    //
    // is not mandatory in publikator, but for datacite it is. we use a fixes publisher here,
    // DARIAH-DE is publishing to DataCite in our case here.
    resource.setPublisher(doiPublisher);

    // publication year
    //
    // get the current year because we publish it now
    Date date = new Date();
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    String year = String.valueOf(calendar.get(Calendar.YEAR));
    resource.setPublicationYear(year);

    // creators
    //
    Creators creators = factory.createResourceCreators();
    for (String creatorText : metadata.getCreator()) {
      Creator creator = factory.createResourceCreatorsCreator();
      creator.setCreatorName(creatorText);
      creators.getCreator().add(creator);
    }
    resource.setCreators(creators);

    // format
    //
    Formats formats = factory.createResourceFormats();
    for (String formatString : metadata.getFormat()) {
      formats.getFormat().add(formatString);
    }
    if (formats.getFormat().size() > 0) {
      resource.setFormats(formats);
    }

    // resourceType
    //
    // mandatory on datacite, but not with DC:type, also must only occur once, we set to Dataset or
    // Collection (depending on dc:format)
    // TODO Map correctly using ADM_MD dcterms:type!?
    // Set to Dataset (default).
    String resourceTypeDesc = "Dataset";
    ResourceType resourceTypeGeneral = ResourceType.DATASET;
    // Set to Collection if format contains [text/vnd.dariah.dhrep.collection+turtle].
    if (formats.getFormat().contains(DARIAH_COLLECTION)) {
      resourceTypeDesc = "Collection";
      resourceTypeGeneral = ResourceType.COLLECTION;
    }

    LOG.info("resourceTypeGeneral: " + resourceTypeGeneral.name() + " (" + resourceTypeDesc + ")");

    if (metadata.getType().size() > 0) {
      resourceTypeDesc = metadata.getType().get(0);
      for (ResourceType type : ResourceType.values()) {
        // yes, in the generated enum there is something similar
        // already, but we want to make it case insensitive
        if (type.value().toLowerCase().equals(resourceTypeDesc.toLowerCase()))
          resourceTypeGeneral = type;
      }
    }
    info.textgrid.middleware.pid.doi.generated.Resource.ResourceType resourceType =
        factory.createResourceResourceType();
    resourceType.setValue(resourceTypeDesc);
    resourceType.setResourceTypeGeneral(resourceTypeGeneral);
    resource.setResourceType(resourceType);

    // now we need to map the DC metadata to the datacite metadata schema as best as we can.
    // if there is a limit we note it by a comment.

    // contributor
    //
    Contributors contributors = factory.createResourceContributors();
    for (String contributorString : metadata.getContributor()) {
      Contributor contributor = factory.createResourceContributorsContributor();
      contributor.setContributorName(contributorString);
      contributor.setContributorType(ContributorType.OTHER);
      contributors.getContributor().add(contributor);
    }
    if (contributors.getContributor().size() > 0)
      resource.setContributors(contributors);

    // coverage / geolocation
    //
    GeoLocations geolocations = factory.createResourceGeoLocations();
    for (String coverage : metadata.getCoverage()) {
      GeoLocation geolocation = factory.createResourceGeoLocationsGeoLocation();
      geolocation.setGeoLocationPlace(coverage);
      geolocations.getGeoLocation().add(geolocation);
    }
    if (geolocations.getGeoLocation().size() > 0) {
      resource.setGeoLocations(geolocations);
    }

    // date
    //
    Dates dates = factory.createResourceDates();
    for (String dateString : metadata.getDate()) {
      info.textgrid.middleware.pid.doi.generated.Resource.Dates.Date dateItem =
          factory.createResourceDatesDate();
      dateItem.setValue(dateString);
      // we have to set a date type, created could make sense, we don't know
      dateItem.setDateType(DateType.CREATED);
      dates.getDate().add(dateItem);
    }
    if (dates.getDate().size() > 0) {
      resource.setDates(dates);
    }

    // description, decriptionType is mandatory, too, we set it to Other
    //
    Descriptions descriptions = factory.createResourceDescriptions();
    for (String descriptionText : metadata.getDescription()) {
      Description description = factory.createResourceDescriptionsDescription();
      description.getContent().add(descriptionText);
      description.setDescriptionType(DescriptionType.OTHER);
      descriptions.getDescription().add(description);
    }
    if (descriptions.getDescription().size() > 0) {
      resource.setDescriptions(descriptions);
    }

    // related resource identifier, here we set the HANDLE ID
    //
    RelatedIdentifiers relatedIdentifiers = factory.createResourceRelatedIdentifiers();
    RelatedIdentifier relatedIdentifier =
        factory.createResourceRelatedIdentifiersRelatedIdentifier();
    relatedIdentifier.setValue(metadata.getHdl());
    relatedIdentifier.setRelatedIdentifierType(RelatedIdentifierType.HANDLE);
    relatedIdentifier.setRelationType(RelationType.IS_IDENTICAL_TO);
    relatedIdentifiers.getRelatedIdentifier().add(relatedIdentifier);

    // while we are he we can set the DC:relation items
    //
    for (String relatedString : metadata.getRelation()) {
      RelatedIdentifier relatedItem = factory.createResourceRelatedIdentifiersRelatedIdentifier();
      relatedItem.setValue(relatedString);
      // FIXME: we don't know, URN seems very generic as type
      relatedItem.setRelatedIdentifierType(RelatedIdentifierType.URN);
      // FIXME: we don't know, has part sounds generic
      relatedItem.setRelationType(RelationType.HAS_PART);
      relatedIdentifiers.getRelatedIdentifier().add(relatedItem);
    }
    // and we can map the source items
    //
    for (String sourceString : metadata.getSource()) {
      RelatedIdentifier relatedItem = factory.createResourceRelatedIdentifiersRelatedIdentifier();
      relatedItem.setValue(sourceString);
      // FIXME: we don't know, URN seems very generic as type
      relatedItem.setRelatedIdentifierType(RelatedIdentifierType.URN);
      relatedItem.setRelationType(RelationType.IS_DERIVED_FROM);
      relatedIdentifiers.getRelatedIdentifier().add(relatedItem);
    }

    resource.setRelatedIdentifiers(relatedIdentifiers);

    // language, in datacite we may only set one language
    //
    if (metadata.getLanguage().size() > 0) {
      String language = metadata.getLanguage().get(0);
      if (!language.isEmpty()) {
        // Check language for correct DataCite content:
        // ([a-zA-Z]{1,8})(-[a-zA-Z0-9]{1,8})*
        // or use rather RFC 1766 (xs:language) as stated by DataCite
        // ([a-zA-Z]{2}|[iI]-[a-zA-Z]+|[xX]-[a-zA-Z]{1,8})(-[a-zA-Z]{1,8})*
        if (language.matches("([a-zA-Z]{2}|[iI]-[a-zA-Z]+|[xX]-[a-zA-Z]{1,8})(-[a-zA-Z]{1,8})*")) {
          LOG.info("language '" + language + "' mapped --> RFC 1766 compliant");
          resource.setLanguage(language);
        } else {
          LOG.warning("language '" + language + "' not mapped --> must be RFC 1766 compliant!");
        }
      }
    }

    // rights
    //
    RightsList rightsList = factory.createResourceRightsList();
    for (String rightsString : metadata.getRights()) {
      Rights right = factory.createResourceRightsListRights();
      right.setValue(rightsString);
      rightsList.getRights().add(right);
    }
    if (rightsList.getRights().size() > 0) {
      resource.setRightsList(rightsList);
    }

    // subject
    //
    Subjects subjects = factory.createResourceSubjects();
    for (String subjectString : metadata.getSubject()) {
      Subject subject = factory.createResourceSubjectsSubject();
      subject.setValue(subjectString);
      subjects.getSubject().add(subject);
    }
    if (subjects.getSubject().size() > 0) {
      resource.setSubjects(subjects);
    }

    // metadata is ready now, we use the JAXB marshaller for XML generation

    JAXBContext jaxbContext = JAXBContext.newInstance("info.textgrid.middleware.pid.doi.generated");
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

    // should we validate the generated XML? This avoids errors before we send it to datacite
    if (validate == true) {
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema =
          sf.newSchema(DataCiteDOIUtil.class.getClassLoader().getResource("metadata.xsd"));
      jaxbMarshaller.setSchema(schema);
    }

    // we have to manually set the schema URL for the resource, else it is
    // missing from the output and the DataCite service will complain
    jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DATACITE_SCHEMA_LOCATION);

    // output pretty printed
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    // jaxbMarshaller.marshal(resource, System.out);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      jaxbMarshaller.marshal(resource, baos);
    } catch (JAXBException e) {
      // TODO We can check errors such as false language tag content here,
      // but we can't repair it here anymore... see 'language'.
    }
    result = new String(baos.toByteArray(), "UTF-8");

    return result;
  }

  /**
   * This goes through a stmtiterator retrieved from model.listProperties, and loops through each
   * item in the iterator. It fills the value into the string array. this works only for strings as
   * datatype.
   * 
   * NOTE now we get the statement's object and transform it into a string. so we get resources
   * (URIs) as strings, too.
   * 
   * @param stmtIt
   * @return array with iterator item values as
   */
  private static List<String> retrieveMetadataElement(StmtIterator stmtIt) {
    List<String> result = new ArrayList<String>();

    while (stmtIt.hasNext()) {
      Statement s = stmtIt.next();
      result.add(getLiteralOrString(s));
    }

    return result;
  }

  /**
   * little helper methods that just concats the DOI prefix with the HDL id which is our doi
   * identifier. using hdl suffix only!
   * 
   * @param hdl
   * @param doiPrefix
   * @return
   */
  public static String getDOIIdentifier(String hdl, String doiPrefix) {
    return doiPrefix + "/" + hdl.substring(hdl.indexOf("/") + 1);
  }

  /**
   * <p>
   * Gets a statement as literal, if literal, string otherwise.
   * </p>
   * 
   * @param theStatement
   * @return
   */
  private static String getLiteralOrString(Statement theStatement) {
    try {
      return theStatement.getObject().asLiteral().getLexicalForm();
    } catch (LiteralRequiredException e) {
      return theStatement.getObject().toString();
    }
  }

}
