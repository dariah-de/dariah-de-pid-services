/**
 * This software is copyright (c) 2025 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Wolfgang Pempe (pempe@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 * @author Johannes Biermann (biermann@sub-uni-goettingen.de)
 */

package info.textgrid.middleware.pid;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import info.textgrid.middleware.pid.api.HandleInfo;
import info.textgrid.middleware.pid.api.HandleMetadata;
import info.textgrid.middleware.pid.api.PidService;
import info.textgrid.middleware.pid.api.PidServiceException;
import info.textgrid.middleware.pid.doi.DataCiteDOIUtil;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;
import net.handle.hdllib.AuthenticationInfo;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.SecretKeyAuthenticationInfo;
import net.handle.hdllib.Util;

/**
 * <p>
 * The PID service implementation.
 * </p>
 * 
 * @author Wolfgang Pempe, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @author Johannes Biermann, SUB Göttingen
 * @version 2025-02-13
 * @since 2011-07-04
 */

public class PidServiceImpl implements PidService {

  // **
  // STATIC
  // **

  private static HandleResolver hdlResolver = new HandleResolver();
  private static AuthenticationInfo authInfo;

  // **
  // CLASS
  // **

  // TextGrid Auth setting.
  private PortTgextra tgauth;

  // Handle Service details.
  private String hsPrefix;
  private String pidSecret = "";

  // DOI server details
  private String doiPrefix;
  private String doiServer;
  private String doiUser;
  private String doiPassword;
  private String doiTargetUrl;
  private String doiPublisher = "";

  // **
  // CONSTRUCTOR
  // **

  /**
   * @param hsPrefix
   * @param hsUser
   * @param hsIndex
   * @param hsPassword
   * @param pidSecret
   * @param tgauth
   * @param logLocation
   * @param doiPrefix
   * @param doiServer
   * @param doiUser
   * @param doiPassword
   * @param doiTargetUrl
   * @param doiPublisher
   * @param logLevel
   * @throws NumberFormatException
   * @throws Exception
   */
  public PidServiceImpl(String hsPrefix,
      String hsUser,
      String hsIndex,
      String hsPassword,
      String pidSecret,
      PortTgextra tgauth,
      String logLocation,
      String doiPrefix,
      String doiServer,
      String doiUser,
      String doiPassword,
      String doiTargetUrl,
      String doiPublisher,
      String logLevel)
      throws NumberFormatException, Exception {

    // Configure logging.
    try {
      PidServiceUtils.startLogging(logLocation, logLevel);
    } catch (IOException e) {
      System.out.println("Could not create logfile " + logLocation + " due to a " + e.getMessage());
      return;
    }

    // Take over values.
    this.pidSecret = pidSecret;
    this.tgauth = tgauth;
    // Get prefixes right here, so we do not have to do it every time!
    this.hsPrefix = PidServiceUtils.getPrefixRight(hsPrefix);
    this.doiPrefix = PidServiceUtils.getPrefixRight(doiPrefix);
    this.doiServer = doiServer;
    this.doiUser = doiUser;
    this.doiPassword = doiPassword;
    this.doiTargetUrl = doiTargetUrl;
    this.doiPublisher = doiPublisher;

    // Create auth info with username, password, and admin index.
    authInfo = new SecretKeyAuthenticationInfo(
        Util.encodeString(this.hsPrefix + hsUser),
        Integer.parseInt(hsIndex),
        Util.encodeString(hsPassword),
        true);

    PidServiceUtils.LOG.info("auth type: " + Util.decodeString(authInfo.getAuthType()));
    PidServiceUtils.LOG.info("auth info: " + authInfo.toString());
    PidServiceUtils.LOG.info("hspassword: " + (hsPassword == null ? "NULL" : "***"));
    PidServiceUtils.LOG.info("pid secret: " + (pidSecret == null ? "NULL" : "***"));
    PidServiceUtils.LOG.info("tgauth: " + (tgauth == null ? "NULL" : tgauth.hashCode()));
    PidServiceUtils.LOG.info("logfile location: " + logLocation + "[" + logLevel + "]");
    PidServiceUtils.LOG.info("doi server: " + doiServer);
    PidServiceUtils.LOG.info("doi prefix: " + doiPrefix);
    PidServiceUtils.LOG.info("doi user: " + (doiUser == null ? "NULL" : doiUser));
    PidServiceUtils.LOG.info("doi password: " + (doiPassword == null ? "NULL" : "***"));
    PidServiceUtils.LOG.info("doi target URL: " + (doiTargetUrl == null ? "NULL" : doiTargetUrl));
    PidServiceUtils.LOG.info("doi publisher: " + this.doiPublisher);
  }

  /**
   * 
   */
  @Override
  public String version() {
    return PidServiceVersion.BUILDNAME + "-"
        + PidServiceVersion.VERSION + "+"
        + PidServiceVersion.BUILDDATE;
  }

  /**
  *
  */
  @Override
  public HandleInfo getTextgridPid(final HandleInfo info, final String sid)
      throws PidServiceException {

    HandleInfo result;

    PidServiceUtils.LOG.info("#GETTEXTGRIDPID");

    // Check access.
    if (!PidServiceUtils.checkAccess(this.tgauth, sid)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    // Get PID.
    result =
        PidServiceUtils.getSingleTextgridPid(hdlResolver, this.hsPrefix, this.version(), info,
            authInfo);

    return result;
  }

  /**
  *
  */
  @Override
  public List<HandleInfo> getTextgridPids(final List<HandleInfo> infoList, final String sid)
      throws PidServiceException {

    List<HandleInfo> result = new ArrayList<HandleInfo>();

    PidServiceUtils.LOG.info("#GETTEXTGRIDPIDS");

    // Check access.
    if (!PidServiceUtils.checkAccess(this.tgauth, sid)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    // Loop HandleInfo and create new PIDs.
    for (HandleInfo info : infoList) {
      result.add(PidServiceUtils.getSingleTextgridPid(hdlResolver, this.hsPrefix, this.version(),
          info, authInfo));
    }

    return result;
  }

  /**
   * @throws PidServiceException
   */
  @Override
  public HandleInfo getDariahPid(final HandleInfo info, final String auth)
      throws PidServiceException {

    HandleInfo result;

    PidServiceUtils.LOG.info("#GETDARIAHPID");

    // Check access.
    if (!PidServiceUtils.checkAccess(this.pidSecret, auth)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    // Check for suffix.
    if (info.suffix == null || info.suffix.isBlank()) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.SUFFIX_ERROR);
    }

    result = new HandleInfo(info.suffix, info.metadata.url);

    // Create default Handle metadata values for DARIAH-DE PID.
    HandleValue values[] = PidServiceUtils.getDefaultDhrepValues(authInfo, this.version());

    // Assemble PID.
    String pid = this.hsPrefix + info.suffix;

    // Create PID.
    try {
      String generatedPid = PidServiceUtils.createSelfGeneratedHandle(
          hdlResolver,
          pid,
          values,
          authInfo);

      if (generatedPid == null || generatedPid.isBlank()) {
        throw PidServiceUtils.newTGPidError(PidServiceUtils.PID_GENERATION_ERROR);
      }

      result.handle = generatedPid;

      PidServiceUtils.LOG.fine("PID created: " + result.handle);

    } catch (HandleException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public String getDariahDoi(final String pid,
      final String doiMeta,
      final String auth)
      throws PidServiceException {

    // FIXME Implement using native Java HTTP clients!!

    String result;

    PidServiceUtils.LOG.info("#GETDARIAHDOI: " + pid);

    // Check access.
    if (!PidServiceUtils.checkAccess(this.pidSecret, auth)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    result = DataCiteDOIUtil.getDOIIdentifier(pid, this.doiPrefix);

    PidServiceUtils.LOG.fine("Converting RDF metadata object to datacite XML metadata object");
    PidServiceUtils.LOG.finest("Input");
    PidServiceUtils.LOG.finest(doiMeta);

    String xmlMetadata = null;
    try {
      xmlMetadata = DataCiteDOIUtil.convertMetadataToDataciteXML(
          doiMeta,
          pid,
          true,
          this.doiPrefix,
          this.doiPublisher);

      PidServiceUtils.LOG.finest("Output");
      PidServiceUtils.LOG.finest(xmlMetadata);

    } catch (Exception e) {
      String message = "Error converting RDF to XML metadata scheme: " + e.getClass().getName()
          + " [" + e.getMessage() + "]";
      throw PidServiceUtils.newTGPidError(message);
    }

    // Create the HTTP Client with the DataSite authentication information
    CredentialsProvider provider = new BasicCredentialsProvider();
    UsernamePasswordCredentials credentials =
        new UsernamePasswordCredentials(this.doiUser, this.doiPassword);
    provider.setCredentials(AuthScope.ANY, credentials);

    HttpClient client =
        HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

    // first register metadata, then DOI
    String metadataRequest = this.doiServer + "/metadata";
    HttpPost postRequest = new HttpPost(metadataRequest);

    StringEntity xml = null;
    xml = new StringEntity(xmlMetadata, "UTF-8");
    xml.setContentType("application/xml;charset=UTF-8");

    PidServiceUtils.LOG.fine("DOI POST request: " + metadataRequest);
    PidServiceUtils.LOG.fine("XML metadata: " + xmlMetadata);

    postRequest.setEntity(xml);

    HttpResponse response;
    try {
      response = client.execute(postRequest);

      PidServiceUtils.LOG
          .fine("DOI service HTTP status code: " + response.getStatusLine().getStatusCode() + " "
              + response.getStatusLine().getReasonPhrase());

      // if the HTTP status is not 201 we had a problem with creating the metadata
      if (response.getStatusLine().getStatusCode() != Status.CREATED.getStatusCode()) {
        throw new WebApplicationException(PidServiceUtils.handleHTTPRequestError(response,
            "Error in registering datacite metadata: "));
      }
    } catch (ClientProtocolException e) {
      throw PidServiceUtils.newTGPidError(e);
    } catch (IOException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    postRequest.releaseConnection();

    PidServiceUtils.LOG.info("Registered DOI metadata, now registering DOI " + result);

    postRequest = new HttpPost(this.doiServer + "/doi");

    StringEntity doiEntity = null;
    // Set the target URL for the DOI. It is configurable via the pid.properties
    String targetUrl = this.doiTargetUrl.replace("#ID", pid);

    PidServiceUtils.LOG.info("target URL: " + targetUrl);

    try {
      doiEntity = new StringEntity("doi=" + result + "\nurl=" + targetUrl);
      doiEntity.setContentType("text/plain;charset=UTF-8");
      postRequest.setEntity(doiEntity);
    } catch (UnsupportedEncodingException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    try {
      response = client.execute(postRequest);

      PidServiceUtils.LOG
          .fine("DOI service HTTP status code: " + response.getStatusLine().getStatusCode() + " "
              + response.getStatusLine().getReasonPhrase());

      // if the HTTP status is not 201 we had a problem with creating the DOI
      if (response.getStatusLine().getStatusCode() != Status.CREATED.getStatusCode()) {
        throw new WebApplicationException(PidServiceUtils.handleHTTPRequestError(response,
            "Error in registering datacite DOI: "));
      }
    } catch (ClientProtocolException e) {
      throw PidServiceUtils.newTGPidError(e);
    } catch (IOException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    postRequest.releaseConnection();

    return result;
  }

  /**
   *
   */
  @Override
  public String getTextgridDoi(String sid,
      String pidSuffix,
      String doiMeta) {

    String result = "";

    PidServiceUtils.LOG.info("#GETTEXTGRIDDOI");

    // TODO Do implement soon!!

    return result;
  }

  // /**
  // * @throws TGPidServiceException
  // */
  // @Override
  // public String getDariahPid(final String auth)
  // throws TGPidServiceException {
  //
  // String result;
  //
  // TGPidServiceUtils.LOG.info("#GETDARIAHPID");
  //
  // // Check access.
  // if (!TGPidServiceUtils.checkAccess(this.pidSecret, auth)) {
  // throw TGPidServiceUtils.newTGPidError(TGPidServiceUtils.AUTH_ERROR);
  // }
  //
  // // Create default Handle metadata values for DARIAH-DE PID.
  // HandleValue values[] = TGPidServiceUtils.getDefaultDhrepValues(authInfo, this.version());
  //
  // // Create PID.
  // try {
  // result = TGPidServiceUtils.createServerGeneratedHandle(
  // hdlResolver,
  // this.hsPrefix,
  // values,
  // authInfo);
  //
  // if (result == null || result.isEmpty()) {
  // throw TGPidServiceUtils.newTGPidError("Generated PID is null or empty!");
  // }
  //
  // TGPidServiceUtils.LOG.fine("PID CREATED: " + result);
  //
  // } catch (HandleException e) {
  // throw TGPidServiceUtils.newTGPidError(e);
  // }
  //
  // return result;
  // }

  /**
   * 
   */
  @Override
  public boolean publishedDariahPid(final String pid)
      throws PidServiceException {

    boolean result = false;

    PidServiceUtils.LOG.info("#PUBLISHEDDARIAHPID: " + pid);

    try {
      String value = PidServiceUtils.getHandleTypeValue(
          hdlResolver,
          pid,
          HandleMetadata.MDTYPE_DH_PUBLISHED);

      if (value.equals(PidServiceUtils.TRUE)) {
        result = true;
      }

    } catch (IOException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public String getMetadata(final String pid, final String type)
      throws PidServiceException {

    String result;

    PidServiceUtils.LOG.info("#GETHANDLEMETADATA: " + pid);

    try {
      result = PidServiceUtils.getHandleTypeValue(
          hdlResolver,
          pid,
          type);

    } catch (IOException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    return result;
  }

  /**
   * 
   */
  @Override
  public void updateMetadata(final HandleInfo info, final String auth)
      throws PidServiceException {

    PidServiceUtils.LOG.info("#UPDATEMETADATA");

    // Check access.
    if (!PidServiceUtils.checkAccess(this.pidSecret, auth)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    // Check for PID.
    if (info.handle == null || info.handle.isBlank()) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.PID_SYNTAX_ERROR);
    }

    PidServiceUtils.LOG.info("Keys to update: " + info.metadata.toString());

    try {
      // Resolve PID and get existing metadata.
      HandleValue existingValues[] = hdlResolver.resolveHandle(info.handle);

      // Create Handle values from key value pairs and existing metadata.
      HandleValue values[] = PidServiceUtils.getUpdatedHandleValues(existingValues, info);

      // Update metadata for existing PID.
      PidServiceUtils.modifyHandleValues(
          hdlResolver,
          info.handle,
          values,
          authInfo);

    } catch (HandleException e) {
      throw PidServiceUtils.newTGPidError(e);
    }
  }

  /**
   * 
   */
  @Override
  public void addMetadata(final HandleInfo info, final String auth)
      throws PidServiceException {

    PidServiceUtils.LOG.info("#ADDMETADATA");

    // Check access.
    if (!PidServiceUtils.checkAccess(this.pidSecret, auth)) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.AUTH_ERROR);
    }

    // Check for PID.
    if (info.handle == null || info.handle.isBlank()) {
      throw PidServiceUtils.newTGPidError(PidServiceUtils.PID_SYNTAX_ERROR);
    }

    PidServiceUtils.LOG.info("Keys to add: " + info.metadata.toString());

    try {
      // Resolve PID and get existing metadata.
      HandleValue existingValues[] = hdlResolver.resolveHandle(info.handle);

      // Create Handle values from key value pairs and existing metadata.
      HandleValue values[] = PidServiceUtils.getAddedHandleValues(existingValues, info);

      // Add metadata to existing PID.
      PidServiceUtils.addHandleValues(
          hdlResolver,
          info.handle,
          values,
          authInfo);

    } catch (HandleException e) {
      throw PidServiceUtils.newTGPidError(e);
    }
  }

  /**
   *
   */
  @Override
  public List<HandleInfo> getFakePids(final List<HandleInfo> infoList) {

    List<HandleInfo> result = new ArrayList<HandleInfo>();

    PidServiceUtils.LOG.info("#GETFAKEPIDS");

    // Loop incoming HandleInfo object.
    for (HandleInfo info : infoList) {
      HandleInfo newFakePid = new HandleInfo(info.suffix, info.metadata.url);
      newFakePid.handle = this.hsPrefix + info.suffix;

      result.add(newFakePid);
    }

    PidServiceUtils.LOG.fine("Fake PIDs created: " + result);

    return result;
  }

  /**
   * 
   */
  @Override
  public String getUri(final String pid)
      throws PidServiceException {

    String result;

    PidServiceUtils.LOG.info("#GETURI: " + pid);

    try {
      result = PidServiceUtils.getHandleTypeValue(
          hdlResolver,
          pid,
          HandleMetadata.MDTYPE_URL);

    } catch (IOException e) {
      throw PidServiceUtils.newTGPidError(e);
    }

    return result;
  }

}
